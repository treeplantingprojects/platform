FROM node:alpine

ARG ALGOLIA_APP_ID
ARG ALGOLIA_API_KEY
ARG GOOGLE_CLIENT_ID
ARG STAGE

WORKDIR /statics
COPY . ./
RUN npm install
RUN echo STAGE=${STAGE} >> .env
RUN echo ALGOLIA_APP_ID=${ALGOLIA_APP_ID} >> .env
RUN echo ALGOLIA_API_KEY=${ALGOLIA_API_KEY} >> .env
RUN echo GOOGLE_CLIENT_ID=${GOOGLE_CLIENT_ID} >> .env
RUN npm run build

FROM python:3
ARG TPP_VERSION_ARG
ENV TPP_VERSION=${TPP_VERSION_ARG}

ENV PYTHONUNBUFFERED 1
WORKDIR /app
COPY --from=0 /statics/static/main.js ./static/main.js
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . ./
EXPOSE 80
CMD ["bash", "-c", "python manage.py migrate && python manage.py runserver 0.0.0.0:80"] 

# myTPP 3.0 Platform

## Backend development workflow

```
virtualenv env
source env/bin/activate
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```

## Frontend development workflow

```
cp .env.example .env
npm i
npm run dev
```

## Visit page

http://127.0.0.1:8000/

TPP platform todos
---

## Projects
### /projects/PROJECT_ID/subscribers
get project subscribers
```json
{
  "count":  54, 
  "list": [
    {
    "name": "Name of subscriber",
    "image": "https://MEDIA/IMAGES"
    }
  ]
}
```

### /projects/PROJECT_ID/members
#### get project members
```json
{
  "count":  54, 
  "list": [
    {
    "profile_id": 5,
    "name": "Name of member",
    "image": "https://MEDIA/IMAGES",
    "role": "NAME or ID"
    }
  ]
}

```
#### remove project member
should remove a member from a project. 
Keep in mind that one person can have multiple roles in a project.
```json
{
  "user/profile": 5,
  "role": "NAME or ID"
}
```

### /projects/PROJECT_ID/images
#### upload image to project
upload image to project (Not sure if there is a need to send multiple images at once or just call the api for every single image you want to upload).
```json
{
  "image": "IMAGE"
}
```

### /projects/PROJECT_ID/details
#### create project
```json
{
  "name": "My cool project",
  "owner": 2, //user or profile id
  "image": "IMAGE", //static header image
  "description": "Header description text",
  "information": "Information text",
  "created_by": 1, //user or profile id
  "published": true
}
```

#### update project
update project details. Should be like patch method to be able to update following fields from a project:
```json
{
  "name": "My cool project",
  "owner": 2, //user or profile id
  "image": "IMAGE", //static header image
  "description": "Header description text",
  "information": "Information text",
  "published": true
}
```
### /projects/PROJECT_ID/news
#### list published project news ordered by newest
```json
[
  {
  "id": 1,
  "message": "My news",
  "created_by": 2, //user or profile id
  "created_at": "04. März 2021"
  }
]
```

#### create and update a project news
```json
{
  "message": "My news",
  "published": true
}
```
#### delete a project news
I guess it can be deleted by the news id in the url param

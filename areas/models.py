from django.db import models
from django.contrib.auth.models import User


class Area(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField(max_length=500)
    image = models.ImageField(upload_to='projects')
    progress = models.IntegerField(default=0)
    location = models.IntegerField(default=0)
    location_name = models.CharField(max_length=100)
    location_link = models.CharField(max_length=500)
    published = models.BooleanField(default=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    size = models.CharField(max_length=25, default="")

    def __str__(self):
        return self.name

from rest_framework import serializers
from .models import Area


class AreaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Area
        fields = ('id', 'name', 'description', 'image', 'location',
                  'location_name', 'location_link', 'owner', 'size', 'published')

from rest_framework import renderers, routers
from django.urls import path
from . import views
from .views import AreaCreateViewSet, AreaViewSet

router = routers.SimpleRouter(trailing_slash=False)
#router.register(r'areas', AreaViewSet)
router.register(r'areas/', AreaCreateViewSet, basename="Area")

urlpatterns = router.urls + [
    #path('projects/', project_list, name="project_list"),
    #path('projects/<int:pk>/', project_detail, name="project_detail"),
]

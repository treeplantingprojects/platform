from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets, generics
from .serializers import AreaSerializer
from .models import Area
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser


class CustomMultiPartParser(MultiPartParser):
    media_type = 'multipart/*'


class AreaViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Area.objects.all()
    serializer_class = AreaSerializer


class AreaCreateViewSet(viewsets.ModelViewSet):
    parser_classes = (CustomMultiPartParser, FormParser, FileUploadParser)
    serializer_class = AreaSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return Area.objects.filter(owner=user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

'''Use this for development'''

from .base import *
import os

ALLOWED_HOSTS += ['127.0.0.1', 'localhost', '192.168.188.96']
DEBUG = True

WSGI_APPLICATION = 'home.wsgi.dev.application'

ALGOLIA = {
    'APPLICATION_ID': '5WACQWVB9Q',
    'API_KEY': '2f9a369b774c1f7af456077098e5a02e'
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

CORS_ORIGIN_WHITELIST = (
    'http://localhost:3000',
)

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

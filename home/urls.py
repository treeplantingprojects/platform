from django.contrib import admin
from django.urls import path, include, re_path
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static
from .views import GoogleLogin, Logout
from allauth.account.views import confirm_email

urlpatterns = [
    path('api-auth/', include('rest_framework.urls')),
    path('api/api-auth-logout', Logout.as_view()),
    path('api/rest-auth/', include('rest_auth.urls')),
    path('api/rest-auth/google/', GoogleLogin.as_view()),
    path('api/', include('projects.urls')),
    path('api/', include('areas.urls')),
    path('api/', include('profiles.urls')),
    path('api/rest-auth/registration/', include('rest_auth.registration.urls')),
    path('admin/', admin.site.urls),
    #re_path(r'^api/auth/', include('djoser.urls')),
    re_path(r'^api/auth/', include('djoser.urls')),
    re_path(r'^api/auth/', include('djoser.urls.authtoken')),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL,
                      document_root=settings.STATIC_ROOT)

urlpatterns += re_path(r'^$', TemplateView.as_view(template_name='index.html')),
urlpatterns += re_path(r'^((?!.*api.*).)*', TemplateView.as_view(template_name='index.html')),
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    current_position = models.CharField(max_length=64)
    about_you = models.CharField(max_length=255, blank=True, default='')
    image = models.ImageField(upload_to='profiles', default='profiles/avatar.jpg')
    user = models.OneToOneField(User,
                                on_delete=models.CASCADE)

    def __str__(self):
        return self.user.first_name+' '+self.user.last_name
    # class Meta:
    #     ordering = ('created',)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

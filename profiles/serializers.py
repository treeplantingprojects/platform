from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Profile


class UserSerializer(serializers.HyperlinkedModelSerializer):
    profile_url = serializers.HyperlinkedIdentityField(
        view_name='profile-detail')

    class Meta:
        model = User
        depth = 1
        fields = ('url', 'id', 'username', 'first_name', 'last_name', 'email',
                  'is_superuser', 'is_staff', 'profile', 'profile_url')


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    #user_url = serializers.HyperlinkedIdentityField(view_name='user-detail')
    user = serializers.ReadOnlyField(source='user.id')
    id = serializers.IntegerField(source='pk', read_only=True)
    username = serializers.CharField(source='user.username', read_only=True)
    email = serializers.CharField(source='user.email')
    first_name = serializers.CharField(source='user.first_name')
    last_name = serializers.CharField(source='user.last_name')
    is_staff = serializers.BooleanField(source='user.is_staff')
    image_url = serializers.SerializerMethodField('get_avatar_url')

    class Meta:
        model = Profile
        depth = 1
        fields = ('id', 'username', 'email', 'first_name', 'last_name',
                  'current_position', 'is_staff', 'user', 'image', 'image_url')

    def get_full_name(self, obj):
        request = self.context['request']
        return request.user.get_full_name()

    def get_avatar_url(self, obj):
        user = obj.user
        social = user.socialaccount_set.filter(provider='google')
        return social[0].extra_data['picture'] if 'avatar.jpg' in obj.image.url and social.exists() else obj.image.url

    def update(self, instance, validated_data):
        # retrieve the User
        user_data = validated_data.pop('user', None)
        for attr, value in user_data.items():
            setattr(instance.user, attr, value)

        # retrieve Profile
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.user.save()
        instance.save()
        return instance

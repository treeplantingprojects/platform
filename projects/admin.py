from django.contrib import admin
from .models import Project, ProjectImage, ProjectArea, ProjectMember, BlogPost, ProjectNews
admin.site.register(Project)
admin.site.register(ProjectImage)
admin.site.register(ProjectArea)
admin.site.register(ProjectMember)
admin.site.register(BlogPost)
admin.site.register(ProjectNews)

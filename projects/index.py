
from algoliasearch_django import AlgoliaIndex
from algoliasearch_django.decorators import register
import os

from .models import Project

STAGE = os.getenv('STAGE', 'development')

@register(Project)
class ProjectIndex(AlgoliaIndex):
    fields = ('id', 'name', 'location', 'location_name', 'image_url', 'progress', 'subscribers_count')
    settings = {'searchableAttributes': ['name', 'location_name', 'location']}
    index_name = f'{STAGE}_Project'
    should_index = 'is_published'

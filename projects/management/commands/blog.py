from django.core.management.base import BaseCommand, CommandError
import requests
import html
from projects.models import BlogPost
from datetime import datetime
from django.utils import timezone



class Command(BaseCommand):
    help = 'Get all TPP wordpress blog posts'

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Fetching blog posts'))
        posts = requests.get("https://treeplantingprojects.com/wp-json/wp/v2/posts?per_page=100")
        self.stdout.write(self.style.SUCCESS('Deleting old blog posts'))
        BlogPost.objects.all().delete()
        for post in posts.json():
            get_image_url = post['_links']['wp:featuredmedia'][0]['href']
            try:
                image_url = requests.get(get_image_url).json()['media_details']['sizes']['meccarouselthumb']['source_url']
            except KeyError:
                image_url = ""
            title = post['title']['rendered']
            date = datetime.strptime(post['date'], '%Y-%m-%dT%H:%M:%S')
            post_url = post['link']
            description = post['excerpt']['rendered']
            print(f'Importing {title}')
            BlogPost(title=title, date=date, description=description, image=image_url, link=post_url).save()

        self.stdout.write(self.style.SUCCESS(f'Successfully imported {len(posts.json())} blog posts'))

from django.db import models
from profiles.models import Profile
from areas.models import Area
from datetime import datetime



class Project(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField(max_length=500)
    image = models.ImageField(upload_to='projects',
                              default='projects/DSC01881.jpg')
    information = models.TextField(max_length=5000)
    progress = models.IntegerField(default=0)
    location = models.IntegerField(default=0)
    location_name = models.CharField(max_length=100)
    published = models.BooleanField(default=False)
    subscribers = models.ManyToManyField(Profile, related_name="project_subscribers", blank=True)
    areas = models.ManyToManyField(Area, related_name="project_areas", blank=True)

    def __str__(self):
        return self.name

    @property
    def image_url(self):
        """Get upload_to path specific to this photo: Algolia recognizable format."""
        return self.image.url

    @property
    def subscribers_count(self):
        return self.subscribers.count()

    @property
    def is_published(self):
        return self.published


class ProjectImage(models.Model):
    image = models.ImageField(upload_to='projects', default='projects/DSC01881.jpg')
    project = models.ForeignKey(Project, on_delete=models.CASCADE)


class ProjectNews(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    image = models.ImageField(upload_to='projects',
                            default='projects/DSC01881.jpg', max_length=500)
    published = models.BooleanField(default=False)
    date = models.DateTimeField(default=datetime.now)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)


class MemberRole(models.IntegerChoices):
    HELPER = 1
    REPORTER = 2
    ORGA = 3


class ProjectMember(models.Model):
    role = models.SmallIntegerField(choices=MemberRole.choices, default=MemberRole.HELPER)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    verified = models.BooleanField(default=False)

    class Meta:
        unique_together = (
            ("role", "profile", "project"),
        )

    def __str__(self):
        return self.profile.user.first_name+' '+self.profile.user.last_name


class ProjectArea(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    def __str__(self):
        return self.area.name


class BlogPost(models.Model):

    def __str__(self):
        return self.title

    title = models.TextField(max_length=150)
    description = models.TextField(max_length=500)
    image = models.ImageField(upload_to='projects',
                              default='projects/DSC01881.jpg', max_length=500)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField(default=datetime.now)
    link = models.URLField(max_length=250)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, blank=True, null=True)

from rest_framework import serializers
from .models import Project, ProjectImage, ProjectArea, ProjectNews, ProjectMember, BlogPost
from areas.serializers import AreaSerializer


class ProjectListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'name', 'progress', 'location_name',
                  'image', 'location')


class ProjectDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'name', 'description', 'progress', 'location_name',
                  'image', 'location', 'information')


class ProjectImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectImage
        fields = ('image', 'project')


class ProjectAreasSerializer(serializers.ModelSerializer):
    area = AreaSerializer(read_only=True)

    class Meta:
        model = ProjectArea
        fields = ('project', 'area', 'data')

    def get_area_data(self, obj):
        return {
            "image": obj.area.image.url,
            "name": obj.area.name,
            "size": obj.area.size,
            "location_name": obj.area.location_name
        }


class ProjectNewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectNews
        fields = ("id", "title", "body", "published", "project", "date", "image")


class ProjectMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectMember
        fields = ("id", "role", "profile", "project", "verified")


class BlogPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlogPost
        fields = ("id", "title", "description", "link", "author", "date", "image")

from rest_framework import routers
from django.urls import path
from .views import (
    get_version,
    ProjectViewSet,
    ProjectNewsViewSet,
    ProjectMemberViewSet,
    ProjectImagesViewSet,
    ProjectSubscribersListAPI,
    manage_subscription,
    ProjectAreaListAPI,
    manage_area,
    get_blog,
    get_project_member_details,
    BlogPostsListAPI
)


router = routers.SimpleRouter(trailing_slash=False)
router.register(r"projects", ProjectViewSet, basename="project")
router.register(r'projects/(?P<project_id>[0-9]+)/news/', ProjectNewsViewSet, basename="project_news")
router.register(r'projects/(?P<project_id>[0-9]+)/members/', ProjectMemberViewSet, basename="project_member")
router.register(r'projects/(?P<project_id>[0-9]+)/images/', ProjectImagesViewSet, basename="project_image")
router.register(r'blog/', BlogPostsListAPI, basename="project_image")


urlpatterns = router.urls + [
    path("projects/<int:project_id>/subscribers/", ProjectSubscribersListAPI.as_view(), name="project_subscribers"),
    path("projects/<int:project_id>/subscribers/manage/", manage_subscription, name="project_subscription_manage"),
    path("projects/<int:project_id>/areas/", ProjectAreaListAPI.as_view(), name="project_areas"),
    path("projects/<int:project_id>/areas/<int:area_id>/", manage_area, name="project_area_manage"),
    path("version", get_version, name="tpp_version"),
    #path("blog/", get_blog, name="tpp_blog"),
    path("project_mebers_details/<int:project_id>", get_project_member_details, name="project_members_details")

]
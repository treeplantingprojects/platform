from rest_framework.permissions import IsAuthenticated, BasePermission
from rest_framework.exceptions import NotFound
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status

from areas.models import Area
from areas.serializers import AreaSerializer

from profiles.models import Profile
from profiles.serializers import ProfileSerializer

from .models import Project, ProjectImage, ProjectMember, ProjectNews, BlogPost
from .serializers import (
    ProjectListSerializer, ProjectImagesSerializer,
    ProjectDetailSerializer, ProjectNewsSerializer, ProjectMemberSerializer,
    BlogPostSerializer
)

from rest_framework.generics import ListAPIView

from rest_framework.viewsets import ModelViewSet

import os
import requests


class AuthenticatedSubscribed(BasePermission):
    def has_permission(self, request, view):
        if request.GET.get('subscribed', None) == "true" and not bool(request.user and request.user.is_authenticated):
            return False
        return True


class ProjectViewSet(ModelViewSet):
    permission_classes = (AuthenticatedSubscribed, )

    def get_serializer_class(self):
        if self.action == "retrieve":
            return ProjectDetailSerializer
        return ProjectListSerializer

    def get_queryset(self):
        project_qs = Project.objects.filter(published=True)
        if self.request.GET.get("subscribed", None) == "true":
            profile = Profile.objects.get(user=self.request.user)
            return project_qs.filter(
                subscribers=profile
            )
        return project_qs


class ProjectNewsViewSet(ModelViewSet):
    serializer_class = ProjectNewsSerializer

    def get_queryset(self):
        return ProjectNews.objects.filter(project_id=self.kwargs["project_id"])

    def perform_create(self, serializer):
        serializer.save(project_id=self.kwargs["project_id"])


class ProjectMemberViewSet(ModelViewSet):
    serializer_class = ProjectMemberSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return ProjectMember.objects.filter(project_id=self.kwargs["project_id"])
        return ProjectMember.objects.filter(project_id=self.kwargs["project_id"], verified=True)

    def perform_create(self, serializer):
        serializer.save(project_id=self.kwargs["project_id"])


class ProjectImagesViewSet(ModelViewSet):
    serializer_class = ProjectImagesSerializer
    queryset = ProjectImage.objects.all()

    def get_queryset(self):
        return ProjectImage.objects.filter(project_id=self.kwargs["project_id"])


class ProjectSubscribersListAPI(ListAPIView):
    serializer_class = ProfileSerializer

    def get_queryset(self):
        return Project.objects.get(pk=self.kwargs["project_id"]).subscribers.all()


class ProjectAreaListAPI(ListAPIView):
    serializer_class = AreaSerializer

    def get_queryset(self):
        return Project.objects.get(pk=self.kwargs["project_id"]).areas.all()


class BlogPostsListAPI(ModelViewSet):
    queryset = BlogPost.objects.all()
    serializer_class = BlogPostSerializer


@api_view(["POST", "DELETE"])
@permission_classes((IsAuthenticated, ))
def manage_area(request, project_id, area_id):
    try:
        area = Area.objects.get(id=area_id)
        project = Project.objects.get(id=project_id)
    except (Area.DoesNotExist, Project.DoesNotExist):
        raise NotFound

    if request.method == "POST":
        project.areas.add(area)
        return Response(data={"ok": True}, status=status.HTTP_201_CREATED)

    project.areas.remove(area)
    return Response(data={"ok": True}, status=status.HTTP_204_NO_CONTENT)


@api_view(["GET"])
def get_version(request):
    return Response(data={"version": os.getenv('TPP_VERSION', 'local build')}, status=200)


@api_view(["POST", "DELETE"])
@permission_classes((IsAuthenticated, ))
def manage_subscription(request, project_id):
    try:
        profile = Profile.objects.get(user=request.user)
        project = Project.objects.get(id=project_id)
    except (Profile.DoesNotExist, Project.DoesNotExist):
        raise NotFound
    if request.method == "POST":
        project.subscribers.add(profile)
        return Response(data={"ok": True}, status=status.HTTP_201_CREATED)
    project.subscribers.remove(profile)
    return Response(data={"ok": True}, status=status.HTTP_204_NO_CONTENT)



@api_view(["GET"])
def get_project_member_details(request, project_id):
    try:
        project = Project.objects.get(id=project_id)
    except (Project.DoesNotExist):
        raise NotFound
    results = []
    user = request.user
    members = ProjectMember.objects.filter(project=project, verified=True)
    try:
        if user.is_authenticated:
            members = members.exclude(profile=user.profile)
    except:
        members = members
    user_members = ProjectMember.objects.filter(project=project, profile=user.profile) if user.is_authenticated else []
    for i in members:
        results.append({
            "name": i.profile.user.first_name+' '+i.profile.user.last_name,
            "image": i.profile.image.url,
            "role": i.role,
        })
    for i in user_members:
        results.append({
            "id": i.profile.id,
            "name": i.profile.user.first_name + ' ' + i.profile.user.last_name,
            "image": i.profile.image.url,
            "role": i.role,
            "verified": i.verified
        })
    print(results)
    return Response(results, status=200)


@api_view(["GET"])
def get_blog(request):
    posts = requests.get("https://treeplantingprojects.com/wp-json/wp/v2/posts")
    tpp_posts = []

    for post in posts.json():
        get_image_url = post['_links']['wp:featuredmedia'][0]['href']
        image_url = requests.get(get_image_url).json()['guid']['rendered']
        title = post['title']['rendered']
        date = post['date']
        post_url = post['link']
        tpp_posts.append({
            'title': title,
            'date': date,
            'image': image_url,
            'post_url': post_url
        })

    print(tpp_posts)

    return Response(data=tpp_posts, status=200)

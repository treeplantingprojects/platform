import 'react-toastify/dist/ReactToastify.css';
import "./App.css";
import Routes from "./routes";
import { BrowserRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import React from "react";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes />

        <ToastContainer
          position="bottom-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={true}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          pauseOnHover
        />
      </BrowserRouter>
    </div>
  );
}

export default App;

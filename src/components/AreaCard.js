import React from "react";
import style from "../style/projectCard.module.css";
import { Progress } from "antd";

export default function AreaCard({ data }) {
  const {
    image,
    location_name,
    name,
    size,
    progress,
    project,
    published,
  } = data;
  return (
    <div className={style.ProjectCard}>
      <img src={image} alt="" />
      <div className={style.cardContent}>
        {location_name ? (
          <div className="d-flex align-items-center">
            {published ? (
              <div
                style={{
                  backgroundColor: "rgba(93,230,143, 0.1)",
                  color: "#5DE68F",
                }}
              >
                <span>bestätigt</span>
              </div>
            ) : (
              <div
                style={{
                  backgroundColor: "rgba(50,58,53, 0.1)",
                  color: "#323a35",
                }}
              >
                <span>ausstehend</span>
              </div>
            )}
          </div>
        ) : ("")}
        <h1>{name}</h1>
        <p className={style.address}>
          <img src={`/static/images/addressIcon.svg`} alt="" /> {location_name}
        </p>
        <p className={style.project}>
          <img src={`/static/images/grey-project-icon.svg`} alt="" /> {size} Hektar
        </p>

        {progress ? <Progress percent={progress} showInfo={false} /> : ""}
      </div>
    </div>
  );
}

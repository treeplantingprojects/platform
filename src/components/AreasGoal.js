import React from "react";
import style from "../style/goal.module.css";
import { Progress } from "antd";

export default function AreasGoal() {
  return (
    <div className={style.goal}>
      <div className={`d-block  d-md-flex justify-content-between`} style={{position: "relative"}}>
        <img src="/static/images/area-info.jpg" alt="" className={style.coverBg} />
        <div className={style.contentSide}>
          <div className={style.contentHeader}>

          </div>
          <h1 className={style.mainHeading}>
            Flächen
          </h1>
          <div className={style.mainText}>
           Du bist in Besitz einer Fläche, welche du gerne zur Verfügung stellen möchtest? Mit TreePlantingProjects kannst du deine Flächen registrieren und der lokalen Community zur Verfügung stellen. Du musst dich dafür nur anmelden.
          </div>
          <div
            className="d-flex align-items-center"
            style={{ marginTop: "2em" }}
          >
            <a className={`${style.greenBtn} mt-0`} href="/login">
              bei TPP anmelden
            </a>

          </div>
        </div>
        <div className={style.coverImage}>
          <img
            className={""}
            src="/static/images/area-info.jpg"
            alt=""
            style={{ width: "100%", height: "100%" }}
          />
        </div>
      </div>
    </div>
  );
}

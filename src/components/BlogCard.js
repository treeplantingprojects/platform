import React from "react";
import style from "../style/projectCard.module.css";
import { Progress } from "antd";

export default function BlogCard({ post }) {
  const {
    image,
    title,
    date,
    description,
  } = post;
  let lDate = new Date(date).toLocaleDateString('de-DE')
  let img = ""
  if (image !== null) img = 'https:/' + image.split('%3A')[1];
  const slug = description.substring(0, 135) + '...';
  const renderHTML = (rawHTML) => React.createElement("div", { dangerouslySetInnerHTML: { __html: rawHTML } });

  return (
    <div className={style.BlogCard}>
      <img src={img} alt="" />
      <div className={style.cardContent}>
        <h1>{renderHTML(title)}</h1>
        {description ? (
          <p>{renderHTML(slug)}</p>
        ) : ("")}
        {date ? (
          <p className={style.project}>
            <img src="/static/images/calendar.svg" alt="" /> {lDate}
          </p>
        ) : (
          ""
        )}
        {/* <p className={style.project}>
            <img src="/static/images/blogIconWhite.svg" alt=""/> von AUTHOR
          </p> */}
      </div>
    </div>
  );
}

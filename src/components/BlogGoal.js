import React from "react";
import style from "../style/goal.module.css";

export default function BlogGoal({ post }) {
  const img = 'https:/' + post.image.split('%3A')[1];
  const description = post.description.replace('<p>', '').replace('</p>', '');
  let lDate = new Date(post.date).toLocaleDateString('de-DE')
  const renderHTML = (rawHTML) => React.createElement("div", { dangerouslySetInnerHTML: { __html: rawHTML } });

  return (
    <div className={style.goal}>
      <div
        className={`d-block d-md-flex`}
        style={{ position: "relative" }}
      >
        <img src={img} alt="" className={style.coverBg} />
        <div className={style.coverImage} style={{ width: "100%" }}>
          <img
            src={img}
            alt=""
            style={{ width: "100%", height: "100%", objectFit: "cover" }}
          />
        </div>
        <div
          className={style.contentSide}
          style={{ padding: "3em 1em 3em 2em" }}
        >
          <div>
            <div className="d-flex align-items-center mb-2">
              <img src="static/images/image3.png" alt="" />
              <div className={`${style.event} mt-0 ml-2`}>
                <img src="static/images/calendar.svg" alt="" />
                {lDate}
              </div>
            </div>
            <div>
              <a className={style.grayBorder} href="#">
                Neuster Blogartikel
              </a>
            </div>
          </div>
          <h1 className={style.mainHeading}>
            {renderHTML(post.title)}
          </h1>
          <div className={style.mainText}>
            {renderHTML(description)}
          </div>
          <a className={style.greenBtn} href={post.link} target={"_blank"}>
            Mehr lesen
          </a>
        </div>
      </div>
    </div>
  );
}

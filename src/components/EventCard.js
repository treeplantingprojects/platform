import React from "react";
import style from "../style/projectCard.module.css";
import { Progress } from "antd";

export default function EventCard({ data }) {
  const { coverImage, tag1, tag2, title, date, dateIcon } = data;
  return (
    <div className={style.ProjectCard}>
      <img src={`/images/${coverImage}`} alt="" />
      <div className={style.cardContent}>
        <div className="d-flex align-items-center">
          <img src={`/images/${tag2[0]}`} alt="" />
          <div className="mx-2">{tag1}</div>
          <p className={style.date}>
            <img src={`/images/${dateIcon}`} alt="" /> {date}
          </p>
        </div>
        <h1>{title}</h1>
      </div>
    </div>
  );
}

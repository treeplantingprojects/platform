import React from "react";
import style from "../style/goal.module.css";

export default function EventGoal() {
  return (
    <div className={style.goal}>
      <div
        className={`d-block  d-md-flex align-items-center justify-content-between`}
        style={{position: "relative"}}
      >
        <img src="/images/coverBg.png" alt="" className={style.coverBg} />
          <div className={style.contentSide}>
            <div className={style.contentHeader}>
              <div>
                <a className={style.grayBorder} href="#">
                  Berlin
                </a>
                <a className={style.grayBorder} href="#">
                  <img
                    className={style.smImage}
                    src="/images/image2.png"
                    alt=""
                  />
                  <img
                    className={style.smImage}
                    src="/images/image3.png"
                    alt=""
                  />
                  <img
                    className={`${style.smImage} mr-1`}
                    src="/images/image4.png"
                    alt=""
                  />
                  <span>+46</span>
                </a>
                <a className={style.grayBorder} href="#">
                  28.06.20, 14:00 Uhr
                </a>
              </div>
            </div>
            <h1 className={style.mainHeading}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed.
            </h1>
            <div className={style.mainText}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero eos et accusam et justo duo
              dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
              sanctus est.
            </div>
            <div
              className="d-flex align-items-center"
              style={{ marginTop: "2em" }}
            >
              <a className={`${style.grayBtn} mr-2`} href="#">
                Help in this event
              </a>
              <div className="d-flex align-items-center mr-3">
                <a href="#" className={style.externalLink}>
                  <img
                    src="/images/map-marked-Icon.svg"
                    alt=""
                    className="img-fluid"
                  />
                </a>
                <a href="#" className={style.externalLink}>
                  <img
                    src="/images/calendar-day-Icon.svg"
                    alt=""
                    className="img-fluid"
                  />
                </a>
              </div>
              <div className="d-flex align-items-center">
                <a href="#" className={style.externalLink}>
                  <img
                    src="/images/Facebook-4.svg"
                    alt=""
                    className="img-fluid"
                  />
                </a>
                <a href="#" className={style.externalLink}>
                  <img
                    src="/images/Twitter-4.svg"
                    alt=""
                    className="img-fluid"
                  />
                </a>
                <a href="#" className={style.externalLink}>
                  <img
                    src="/images/YouTube-4.svg"
                    alt=""
                    className="img-fluid"
                  />
                </a>
              </div>
            </div>
          </div>
        <div className={style.coverImage}>
          <img
            className={""}
            src="/images/goalimg.png"
            alt=""
            style={{ width: "100%", height: "100%" }}
          />
        </div>
      </div>
    </div>
  );
}

import React from "react";
import style from "../style/Footer.module.css";

export default function Footer() {
  return (
    <div>
      <section className={style.midSection}>
        <div className={style.customContainer}>
          <h3 className={style.mainHeading}>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr.
          </h3>
          <div className={style.mainText}>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod
          </div>
          <div className={style.searchArea}>
            <input
              className={style.searchInput}
              type="email"
              name=""
              placeholder="Enter your email"
            />
            <button className={style.sendBtn}>
              <img
                src="/images/send.svg"
                alt=""
                style={{ width: "1.8925em" }}
              />
              <span>Start now</span>
            </button>
          </div>
        </div>
      </section>
      <div className={style.mainFooter}>
        <div className={style.customContainer}>
          <div className={"row"}>
            <div class="col-md-10 mx-auto">
              <div className="d-flex align-items-center justify-content-center justify-content-md-start flex-wrap">
                <span className={`${style.mainHeading} mb-3 mb-sm-0`}>
                  TreePlantingProjects
                </span>
                <div className="d-flex align-items-center">
                  <a className={style.socialLink} href="#">
                    <img src="/images/Facebook.svg" alt="" />
                  </a>
                  <a className={style.socialLink} href="#">
                    <img src="/images/Twitter.svg" alt="" />
                  </a>
                  <a className={style.socialLink} href="#">
                    <img src="/images/YouTube.svg" alt="" />
                  </a>
                </div>
              </div>
              <div class="row mt-5 text-center text-md-left">
                <div class="col-md-2">
                  <div className={style.linksLabel}>Lorem</div>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                </div>
                <div class="col-md-2 mt-4 mt-md-0">
                  <div className={style.linksLabel}>Lorem</div>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                </div>
                <div class="col-md-2 mt-4 mt-md-0">
                  <div className={style.linksLabel}>Lorem</div>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                </div>
                <div class="col-md-2 mt-4 mt-md-0">
                  <div className={style.linksLabel}>Lorem</div>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                  <a className={style.footerLink} href="#">
                    lorem
                  </a>
                </div>
                <div class="col-md-4 mt-4 mt-md-0">
                  <div className={style.smText}>
                    The latest Move news, articles, and resources, sent straight
                    to your inbox every month.
                  </div>
                  <form action="">
                    <input
                      className={style.footerInput}
                      type="email"
                      name=""
                      placeholder="Email Address"
                    />
                    <button className={style.mainBtn}>Subscribe</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

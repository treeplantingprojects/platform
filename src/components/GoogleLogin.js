import React, { Component } from 'react';
import GoogleLogin from 'react-google-login';
import { toast } from "react-toastify";
import { googleSocialLogin } from "../store/auth";
import { connect } from "react-redux";

class GoogleSocialAuth extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const onSuccess = (response) => {
      this.props.googleSocialLogin(response.accessToken)
    }
    const onFailure = (response) => {
    }
    return (
      <GoogleLogin
        clientId={process.env.GOOGLE_CLIENT_ID}
        buttonText="oder mit Google"
        onSuccess={onSuccess}
        onFailure={onFailure}
        cookiePolicy={'single_host_origin'}
      />
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    googleSocialLogin: (access_token) => dispatch(googleSocialLogin(access_token)),
  };
};

export default connect(
  null,
  mapDispatchToProps
)(GoogleSocialAuth);

import React, { useState } from "react";
import style from "../style/Header.module.css";
import {Link} from "react-router-dom";

export default function Header({ data }) {
  const { pageLinks, ActionBtn, mainBtn } = data;
  const [link, setLink] = useState(pageLinks[0]);
  return (
  <div className={style.menu}>
    <div style={{position: "sticky", top: "0", zIndex: "2"}}>
    <div
      className={`${style.Header} justify-content-center justify-content-lg-between`}
    >
      <div className="mb-2 mb-md-0">
        {pageLinks.map((item) => (
          <a
            href={`#${item}`}
            className={link == item ? style.active : ""}
            onClick={() => setLink(item)}
          >
            {item}
          </a>
        ))}
      </div>
      <div className="">
        {/*{ActionBtn.map((item) => (*/}
        {/*  <button className={`${style.actionBtn} mb-2 mb-sm-0`}>*/}
        {/*    <img src={`/images/${item.icon}`} alt="" /> {item.title}*/}
        {/*  </button>*/}
        {/*))}*/}
        <Link to={"/projects_join"}>
          <button className="primaryBtn" style={{ padding: "1.2em 3.1em" }}>
            <img src="/static/images/user-plus-solid.svg" alt="" className="mr-2" />{" "}
            {mainBtn}
          </button>
        </Link>
      </div>
    </div>
    </div>
  </div>
  );
}

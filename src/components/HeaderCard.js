import React from "react";
import style from "../style/goal.module.css";
import { Progress } from "antd";

export default function HeaderCard({data}) {
   const {
        title,
        subText,
        button,
        buttonTarget
  } = data;
  return (
    <div className={style.goal}>
      <div className={`d-block  d-md-flex justify-content-between`} style={{position: "relative"}}>
        <img src="/images/coverBg.png" alt="" className={style.coverBg} />
        <div className={style.contentSide}>
          <h1 className={style.mainHeading}>
              {title}
          </h1>
          <div className={style.mainText}>
              {subText}
          </div>
          <div
            className="d-flex align-items-center"
            style={{ marginTop: "2em" }}
          >
            <a className={`${style.greenBtn} mt-0`} href={buttonTarget}>
                {button}
            </a>

          </div>
        </div>
        <div className={style.coverImage}>
          <img
            className={""}
            src="/images/goalimg.png"
            alt=""
            style={{ width: "100%", height: "100%" }}
          />
        </div>
      </div>
    </div>
  );
}

import React from "react";
import style from "../style/goal.module.css";
import { Progress } from "antd";
import {Link} from "react-router-dom";

export default function HomeGoal({authenticated}) {
  return (
    <div className={style.goal}>
      <div className={`d-block  d-md-flex justify-content-between`} style={{position: "relative"}}>
        <img src="/static/images/HomeInfo.jpg" alt="" className={style.coverBg} />
        <div className={style.contentSide}>
          <div className={style.contentHeader}>

          </div>
          <h1 className={style.mainHeading}>
            Willkommen bei TreePlantingProjects
          </h1>
          <div className={style.mainText}>
           Aktuell befindet sich die Plattform noch im Aufbau.
             <br/>Einige Funktionen können noch fehleranfällig sein. <br/><strong>Dies ist eine BETA Version!</strong>
          </div>
          <div
            className="d-flex align-items-center"
            style={{ marginTop: "2em" }}
          >
            {authenticated ? (
                ""
            ):(
              <Link className={`${style.greenBtn} mt-0`} to={"/login"}>
                bei TPP anmelden
              </Link>
            )
            }

          </div>
        </div>
        <div className={style.coverImage}>
          <img
            className={""}
            src="/static/images/HomeInfo.jpg"
            alt=""
            style={{ width: "100%", height: "100%" }}
          />
        </div>
      </div>
    </div>
  );
}

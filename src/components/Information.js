import React from "react";

export default function Information({ data }) {
  const { information, images } = data;
  return (
    <div className="row">
      <div className={images.length > 0 ? ("col-md-8 mb-3 mb-md-0") : ("col-md-12 mb-3 mb-md-0")}>
        <div
          className="bg-white h-100"
          style={{ padding: "1.5em", borderRadius: "2em" }}
        >
          <p
          >
            {
              information.split('\r\n').map(function (item, idx) {
                return (
                  <span key={idx}>
                    {item}
                    <br />
                  </span>
                )
              })
            }
          </p>
        </div>
      </div>
      {images.length > 0 ? (
        <div className="col-md-4 ">
          <div
            className="row bg-white"
            style={{ padding: "1.5em 0", borderRadius: "2em" }}
          >
            <div className="col-12">
              <div>
                <img
                  src={images[0]}
                  alt=""
                  className="img-fluid"
                  style={{ borderRadius: "2em" }}
                />
              </div>
            </div>
            {images.slice(1).map((image => (
              <div className="col-md-4 mt-3">
                <img
                  src={image}
                  alt=""
                  className="img-fluid"
                  style={{ borderRadius: "2em" }}
                />
              </div>
            )))}
          </div>
        </div>
      ) : (
        ""
      )}

    </div>
  );
}

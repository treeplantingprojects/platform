import React from "react";
import style from "../style/createArea.module.css";
import { Link, Redirect } from "react-router-dom";
import { authLogin } from "../store/auth";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import GoogleLogin from "./GoogleLogin";


class Login extends React.Component {
  state = {
    username: "",
    password: ""
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { username, password } = this.state;
    this.props.login(username, password);
  };

  render() {
    const { error, loading, token } = this.props;
    const { username, password } = this.state;
    if (token) {
      return <Redirect to="/" />;
    }
    return (
      <div className={style.form}>
        <div className={`row d-flex vh-100 ${style.sideImage}`}>
          <div className={"col-md-6"}>
            <img className={"img-fluid"} src="/static/images/login_bg.jpg" alt="" />
          </div>
          <div className={"col-md-6 py-4 pr-5 pl-5"}>
            <p onClick={this.props.history.goBack} className={style.backLink}>
              <img src="/static/images/icon-arrow-left.svg" alt="" />
                zurück
              </p>
            <h2 className={style.mainHeading}>Anmelden</h2>
            <div className={"row"}>
              <div className={"col-md-12"}>
                <form className={"mt-5"} action="" onSubmit={this.handleSubmit}>
                  <div className={style.formInput}>
                    <input
                      className={style.commonInput}
                      type="email"
                      onChange={this.handleChange}
                      value={username}
                      required
                      name="username"
                      placeholder="E-Mail-Adresse"
                    />
                    <img
                      src="static/images/users-Icon.svg"
                      className={style.inputIcon}
                      alt=""
                    />
                  </div>
                  <div className={style.formInput}>
                    <input
                      className={style.commonInput}
                      type="password"
                      onChange={this.handleChange}
                      value={password}
                      name="password"
                      required
                      placeholder="Passwort"
                    />
                    <img
                      src="static/images/key-solid.svg"
                      className={style.inputIcon}
                      alt=""
                    />
                  </div>
                  <div className="text-left mt-2 row">
                    <div className={"col-md-8"}>
                      <button type="submit" className={style.formBtn}>
                        <img src="/static/images/sign-in-alt-solid.svg" alt="" className="mr-2" />
                        Anmelden</button>
                    </div>
                    <div className={`col-md-4 ${style.socialLogin}`}>
                      <GoogleLogin />
                    </div>

                  </div>
                </form>
                <div className={`${style.mainText} row`}>
                  <Link to={"/signup"}>
                    <p>Noch keinen Account?</p>
                  </Link>
                  <Link to={"/reset_password"}>
                    <p className={"ml-2"}>Passwort vergessen?</p>
                  </Link>
                </div>
              </div>
            </div>
          </div >
        </div >
      </div >
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    token: state.auth.token !== null
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: (username, password) => dispatch(authLogin(username, password)),
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login));

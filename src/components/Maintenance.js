import React from "react";
import style from "../style/createArea.module.css";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';


class Maintenance extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { maintenance, version } = this.props;
    if (!maintenance) this.props.history.push('/');
    return (
      <div className={style.form}>
        <div className={`row d-flex vh-100 ${style.sideImage}`}>
          <div className={"col-md-6"}>
            <img className={"img-fluid"} src="/static/images/maintenence.jpg" alt="" />
          </div>
          <div className={"col-md-6 py-4 pr-5 pl-5"}>
            <h2 className={style.mainHeading}>Wartung</h2>
            <div className={"row"}>
              <div className={"col-md-12"}>
                <p>
                  Wir aktualisieren gerade unser System.
                  Du wirst automatisch weitergeleitet sobald wir fertig sind.
                </p>
                <p>
                  Im Normalfall dauert dieser Vorgang nicht länger wie eine Minute.
                </p>
                <p>Aktuelle Version: {version}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    maintenance: state.version.maintenance,
    version: state.version.name
  };
};


export default withRouter(
  connect(
    mapStateToProps,
    null
  )(Maintenance));

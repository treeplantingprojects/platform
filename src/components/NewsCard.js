import React from "react";
import style from "../style/projectCard.module.css";
import { Progress } from "antd";

export default function NewsCard({ news }) {
  const {
    image,
    title,
    date,
    description,
  } = news;
  let lDate = new Date(date).toLocaleDateString('de-DE')
  const slug = description.replace('<p>', '').replace('</p>', '').substring(0, 15);
  const renderHTML = (rawHTML) => React.createElement("div", { dangerouslySetInnerHTML: { __html: rawHTML } });

  return (
    <div className={style.BlogCard}>
      <img src={image} alt="" />
      <div className={style.cardContent}>
        <h1>{renderHTML(title)}</h1>
        {description ? (
          <p>{description}</p>
        ) : ("")}
        {date ? (
          <p className={style.project}>
            <img src="/static/images/calendar.svg" alt="" /> {lDate}
          </p>
        ) : (
          ""
        )}
        {/* <p className={style.project}>
            <img src="/static/images/blogIconWhite.svg" alt=""/> von AUTHOR
          </p> */}
      </div>
    </div>
  );
}

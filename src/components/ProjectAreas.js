import React from "react";
import AreaCard from "../components/AreaCard";
import { motion } from "framer-motion";
import { Progress } from "antd";

export default function ProjectAreas({areas}) {
  return (
      <div>
         <div className="row ">
            {areas.map((area => (
                <AreaCard key={area} data={area} />
                )))}
        </div>
    </div>
  );
}

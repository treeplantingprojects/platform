import React from "react";
import style from "../style/projectCard.module.css";
import { Progress } from "antd";

class ProjectCard extends React.Component {
  constructor(props) {
    super(props);
    this.state



  }

  componentDidMount() {
    // fetch project card info
  }

  render() {
    const {
      image,
      location_name,
      name,
      progress,
    } = this.props.data;
    const project = '';
    const projectIcon = '';
    return (
      <div className={style.ProjectCard}>
        <img src={image} alt="" />
        <div className={style.cardContent}>
          <div className="d-flex align-items-center">
            <div>{location_name}</div>
          </div>
          <h1>{name}</h1>
          {project ? (
            <p className={style.project}>
              <img src={`/static/images/${projectIcon}`} alt="" /> {project}
            </p>
          ) : (
            ""
          )}
          {progress ? <Progress percent={progress} showInfo={false} /> : ""}
        </div>
      </div>
    );
  }
}

export default ProjectCard;
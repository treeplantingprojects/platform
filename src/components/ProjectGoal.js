import React from "react";
import style from "../style/goal.module.css";
import { Progress } from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { motion } from "framer-motion";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { subscribeProject, unsubscribeProject } from "../store/projects";


class ProjectGoal extends React.Component {
  constructor(props) {
    super(props);
  };

  handleWhatsapp = () => {
    window.open('whatsapp://send?text=Kennst du schon ' + this.props.data.name + '? \n\nSchau mal hier: ' + window.location.href, "_blank");
  }

  handleFacebook = () => {
    window.open('https://www.facebook.com/sharer/sharer.php?u=' + window.location.href, '_blank')
  }

  render() {
    const { id, image, description, name, progress, location_name } = this.props.data;
    const { authenticated, has_sub } = this.props;
    return (
      <div className={style.goal}>
        <div className={`d-block  d-md-flex justify-content-between`} style={{ position: "relative" }}>
          <img src={image} alt="" className={style.coverBg} style={{ width: "100%", height: "100%", objectFit: "cover" }} />
          <div>
            <div className={style.contentSide}>
              <div className={style.contentHeader}>
                <div>
                  <p className={style.grayBorder}>
                    {location_name}
                  </p>
                  <p className={style.grayBorder}>
                    <img
                      className={style.smImage}
                      src="/static/images/image2.png"
                      alt=""
                    />
                    <img
                      className={style.smImage}
                      src="/static/images/image3.png"
                      alt=""
                    />
                    <img
                      className={`${style.smImage} mr-1`}
                      src="/static/images/image4.png"
                      alt=""
                    />
                    <span>+46</span>
                  </p>
                </div>
              </div>
              <h1 className={style.mainHeading}>
                {name}
              </h1>
              <div className={style.mainText}>
                {
                  description.split('\r\n').map(function (item, idx) {
                    return (
                      <span key={idx}>
                        {item}
                        <br />
                      </span>
                    )
                  })
                }
              </div>
              <div
                className="d-flex align-items-center"
                style={{ marginTop: "2em" }}
              >
                {authenticated ? (
                  has_sub ? (
                    <motion.div whileHover={{ scale: 1.05 }} whileTap={{ scale: 0.9 }} >
                      <button className={`${style.grayBtn} mt-0`} onClick={() => this.props.handleUnsubscribe(id)}>
                        <img src="/static/images/bell-slash-regular.svg" alt="" className="mr-2" />
                        Deabonnieren
                      </button>
                    </motion.div>
                  ) : (
                    <motion.div whileHover={{ scale: 1.05 }} whileTap={{ scale: 0.9 }}>
                      <button className={`${style.greenBtn} mt-0`} onClick={() => this.props.handleSubscribe(id)}>
                        <img src="/static/images/bell-regular.svg" alt="" className="mr-2" />
                        Abonnieren
                      </button>
                    </motion.div>
                  )
                ) : (
                  <div>
                    <Link to="/login" className={`${style.greenBtn} mt-0`}>
                      <img src="/static/images/bell-regular.svg" alt="" className="mr-2" />
                      Abonnieren
                    </Link>
                  </div>
                )
                }
                <div className="d-flex align-items-center">
                  <div className="d-flex align-items-center mr-3">

                  </div>
                  <div className="d-flex align-items-center">
                    <motion.div whileHover={{ scale: 1.2 }} whileTap={{ scale: 0.9 }}>
                      <CopyToClipboard text={window.location.href}>
                        <button className={style.externalLink}>
                          <img
                            src="/static/images/link.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </button>
                      </CopyToClipboard>
                    </motion.div>
                    <button className={style.externalLink} onClick={this.handleWhatsapp}>
                      <img
                        src="/static/images/whatsapp-brands.svg"
                        alt=""
                        className="img-fluid"
                      />
                    </button>
                    <button className={style.externalLink} onClick={this.handleFacebook}>
                      <img
                        src="/static/images/facebook-f-brands.svg"
                        alt=""
                        className="img-fluid"
                      />
                    </button>
                  </div>

                </div>
                {/*<Progress*/}
                {/*  percent={progress}*/}
                {/*  style={{*/}
                {/*    width: "50%",*/}
                {/*    marginLeft: "1.5em",*/}
                {/*    display: "flex",*/}
                {/*    alignItems: "center",*/}
                {/*    showInfo: false*/}
                {/*  }}*/}
                {/*/>*/}
              </div>
            </div>
          </div>
          <div className={style.coverImage}>
            <img
              className=""
              src={image}
              alt=""
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
            />
          </div>

        </div >
      </div >
    );
  }
}



const mapDispatchToProps = dispatch => {
  return {
    getSubState: (projectId) => dispatch(fetchProjectSub(projectId)),
    handleSubscribe: (projectId) => dispatch(subscribeProject(projectId)),
    handleUnsubscribe: (projectId) => dispatch(unsubscribeProject(projectId))

  };
};

export default connect(
  null,
  mapDispatchToProps
)(ProjectGoal);

import React from "react";
import ProjectMembersCard from "./ProjectMembersCard";
import { motion } from "framer-motion";

export default function ProjectMembers({members}) {
  return (
      <div
        style={{
          marginLeft: "-1em",
          display: "flex",
          alignItems: "center",
          flexWrap: "wrap",
        }}
      >


        {members.map((member => (
                <ProjectMembersCard key={member} member={member} />
            )))}
    </div>
  );
}

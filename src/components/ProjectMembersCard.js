import React from "react";
import style from "../style/smallCard.module.css";
export default function ProjectMembersCard({member}) {
    const { name, image, role, verified, id} = member
    let role_name = ""
    if(role == 1) role_name = "Helfer"
    if(role == 2) role_name = "Reporter"
    if(role == 3) role_name = "Organisator"
  return (
    <div className={style.SmallCard}>
      <img src="/static/images/smCardImage.svg" alt="" />
      <div className={style.tooltip}>{role_name}</div>
      <img src={image} alt="" />
      <div>
        <h6>{name}</h6>
        <p>{role_name} {!verified && id? ("- Ausstehend"):("")}</p>
      </div>
    </div>
  );
}

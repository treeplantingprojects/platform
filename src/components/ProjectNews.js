import React from "react";
import AreaCard from "../components/AreaCard";
import { motion } from "framer-motion";
import { Progress } from "antd";
import BlogCard from "./BlogCard";
import NewsCard from "./NewsCard";

export default function ProjectNews({ news }) {
  return (
    <div>
      <div className="row ">
        {news.map((newa => (
          <NewsCard key={newa.id} news={{ title: newa.title, description: newa.body, date: newa.date, image: newa.image }} />
        )))}
      </div>
    </div>
  );
}

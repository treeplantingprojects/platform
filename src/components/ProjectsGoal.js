import React from "react";
import style from "../style/goal.module.css";
import { Progress } from "antd";
import {Link} from "react-router-dom";

export default function ProjectsGoal({authenticated}) {
  return (
    <div className={style.goal}>
      <div className={`d-block  d-md-flex justify-content-between`} style={{position: "relative"}}>
        <img src="/static/images/project-info.jpg" alt="" className={style.coverBg} />
        <div className={style.contentSide}>
          <div className={style.contentHeader}>

          </div>
          <h1 className={style.mainHeading}>
            Projekte
          </h1>
          <div className={style.mainText}>
           Mit TreePlantingProjects kannst du ganz einfach lokale Projekte finden und direkt mitmachen. Wir empfehlen dir Projekte zu abonnieren, damit Du nichts mehr verpasst. Dafür musst du dich nur anmelden. <br/><strong>Dies ist eine BETA Version!</strong>
          </div>
          <div
            className="d-flex align-items-center"
            style={{ marginTop: "2em" }}
          >
            {!authenticated ? (
              <Link to={"/login"} className={`${style.greenBtn} mt-0`}>
                bei TPP anmelden
              </Link>
            ):("")
            }

          </div>
        </div>
        <div className={style.coverImage}>
          <img
            className={""}
            src="/static/images/project-info.jpg"
            alt=""
            style={{ width: "100%", height: "100%" }}
          />
        </div>
      </div>
    </div>
  );
}

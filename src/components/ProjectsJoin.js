import React from "react";
import style from "../style/createArea.module.css";
import {Link, Redirect} from "react-router-dom";
import {authLogin} from "../store/auth";
import {connect} from "react-redux";
import { withRouter } from 'react-router-dom';
import Projects from "../pages/Projects";
import { joinProject } from "../store/projects"


class ProjectsJoin extends React.Component {

  constructor(props) {
    super(props);
  }

  handleClick = e => {
    this.props.joinProject(this.props.project, e, this.props.profile);
    this.props.history.goBack();
  }

  render() {
    const {error, loading, token, projectName} = this.props;
    if (!token) {
      return <Redirect to="/login"/>;
    }
    return (
        <div className={style.form}>
          <div className={`row d-flex vh-100 ${style.sideImage}`}>
            <div className="col-md-6">
              <img className={"img-fluid"} src="/static/images/join_project_bg.jpg" alt=""/>
            </div>
            <div className={"col-md-6 py-4 pr-5 pl-5"}>
              <p onClick={this.props.history.goBack} className={style.backLink}>
                <img src="/static/images/icon-arrow-left.svg" alt=""/>
                zurück
              </p>
              <h2 className={style.mainHeading}>{projectName} beitreten</h2>
              <div className={style.mainText}>Aktuell suchen wir für das Projekt folgende Rollen:</div>
              <div className={"row pt-3"}>
                <div className={"col-md-12"}>
                <a onClick={() => {this.handleClick(1)}}>
                  <div className={style.userInfo}>
                    <div className={style.imageSide}>
                      <div className={style.darkOverlay} />
                      <img src="/static/images/join_helper.jpg" alt="" />
                      <img src="/static/images/Sergei_Filatov.svg" alt="" />
                    </div>
                    <div>
                      <div className={style.subHeading}>Helfer</div>
                      <div className={style.mainText}>Du hillfst bei Aktionen.</div>
                    </div>
                  </div>
                </a>
                </div>

              </div>
              <div className="row pt-3">
                <div className="col-md-12">
                <a onClick={() => {this.handleClick(2)}}>
                  <div className={style.userInfo}>
                    <div className={style.imageSide}>
                      <div className={style.darkOverlay}></div>
                      <img src="static/images/Avatar-2.png" alt=""/>
                      <img src="static/images/Sergei_Filatov.svg" alt=""/>
                    </div>
                    <div>
                      <div className={style.subHeading}>Reporter</div>
                      <div className={style.mainText}>Du berichstest über euer Vorhaben.</div>
                    </div>
                  </div>
                </a>
                </div>

              </div>
              <div className="row pt-3">
                <div className="col-md-12">
                <a onClick={() => {this.handleClick(3)}}>
                  <div className={style.userInfo}>
                    <div className={style.imageSide}>
                      <div className={style.darkOverlay}></div>
                      <img src="/static/images/join_orga.jpg" alt=""/>
                      <img src="/static/images/Sergei_Filatov.svg" alt=""/>
                    </div>
                    <div>
                      <div className={style.subHeading}>Organisator</div>
                      <div className={style.mainText}>Du organisierst Aktionen.</div>
                    </div>
                  </div>
                  </a>
                </div>

              </div>

            </div>
          </div>
        </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    profile: state.profile.profile_id,
    project: state.projects.selected,
    projectName: state.projects.details[state.projects.selected].name,
    error: state.auth.error,
    token: state.auth.token !== null
  };
};

const mapDispatchToProps = dispatch => {
  return {
    joinProject: (id, role, profile) => dispatch(joinProject(id, role, profile))
  };
};

export default withRouter(
    connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectsJoin));

import React from "react";
import style from "../style/createArea.module.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { reportArea } from "../store/areas";


class ReportArea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fileUploadState: "",
      location: "",
      size: "",
      link: "",
      image: null,
      descripton: ""
    }
    this.inputReference = React.createRef();
  }


  fileUploadAction = () => this.inputReference.current.click();
  fileUploadInputChange = (e) => {
    this.setState({ fileUploadState: e.target.value });
    this.setState({ image: e.target.files[0] });
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { location, size, link, image, description } = this.state;
    this.props.report(location, size, link, image, description);
    this.props.history.goBack();
  };

  render() {
    const { location, size, link, image, description } = this.state;
    return (
      <div className={style.form}>
        <div className={`row d-flex vh-100 ${style.sideImage}`}>
          <div className="col-md-6">
            <img className={"img-fluid"} src="static/images/report_area_bg.jpg" alt="" />
          </div>
          <div className={"col-md-6 py-4 pr-5 pl-5"}>
            <Link to="/areas" className={style.backLink}>
              <img src="static/images/icon-arrow-left.svg" alt="" />
                zurück
              </Link>
            <h2 className={style.mainHeading}>Neue Fläche melden</h2>
            <div className={style.mainText}>Wir nehmen deine Fläche in unsere Datenbank auf und stellen sie lokalen Projektgruppen zur Verfügung.
              </div>
            <form className="mt-5" action="" onSubmit={this.handleSubmit}>
              <div className={"row"}>
                <div className={"col-md-6"}>
                  <div className={style.formInput}>
                    <input
                      className={style.commonInput}
                      type="text"
                      onChange={this.handleChange}
                      value={location}
                      name="location"
                      required
                      placeholder="Ort"
                    />
                    <img
                      src="static/images/addressIcon.svg"
                      className={style.inputIcon}
                      alt=""
                    />
                  </div>
                  <div className={style.formInput}>
                    <input
                      className={style.commonInput}
                      type="text"
                      onChange={this.handleChange}
                      value={size}
                      required
                      name="size"
                      placeholder="Größe in Hektar"
                    />
                    <img
                      src="static/images/projectIcon.svg"
                      className={style.inputIcon}
                      alt=""
                    />
                  </div>
                  <div className={style.formInput}>
                    <input
                      className={style.commonInput}
                      type="text"
                      required
                      onChange={this.handleChange}
                      value={link}
                      name="link"
                      placeholder="Bayern Atlas Link"
                    />
                    <img
                      src="static/images/link.svg"
                      className={style.inputIcon}
                      alt=""
                    />
                  </div>
                  <div className={style.formInput}>
                    <input
                      className={style.commonInput}
                      type="text"
                      readOnly="readOnly"
                      required
                      placeholder={this.state.fileUploadState ? (this.state.fileUploadState.replace("C:\\fakepath\\", "")) : ("Bild hochladen")}
                      onClick={this.fileUploadAction}
                    />
                    <input
                      type="file"
                      style={{ display: "none" }}
                      name="image"
                      hidden
                      ref={this.inputReference}
                      onChange={this.fileUploadInputChange}
                    />
                    <img
                      className={style.inputIcon}
                      src="/static/images/image-Icon.svg"
                      alt=""
                    />
                  </div>

                </div>
                <div className={"col-md-6"}>
                  <textarea
                    className={style.commonInput}
                    onChange={this.handleChange}
                    value={description}
                    name="descriptiond"
                    cols="30"
                    rows="10"
                    required
                    placeholder="Beschreibung/Vorhaben"
                  ></textarea>
                </div>
              </div>
              <div className={"text-right mt-5"}>
                <button type={"submit"} className={style.formBtn}>
                  <img src={`static/images/plus-square-Icon.svg`} alt="" className="mr-2" />{" "}
                  Fläche hinzufügen</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    token: state.auth.token
  };
};

const mapDispatchToProps = dispatch => {
  return {
    report: (location, size, link, image, description) => {
      dispatch(reportArea(location, size, link, image, description))
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportArea);

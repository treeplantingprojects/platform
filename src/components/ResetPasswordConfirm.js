import React from "react";
import style from "../style/createArea.module.css";
import { Link, Redirect } from "react-router-dom";
import { resetPasswordConfirm } from "../store/auth";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import PasswordStrengthBar from 'react-password-strength-bar';


class ResetPasswordConfirm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      password2: "",
      id: "",
      token: "",
      validation: false
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    const { id, token, password, password2, passwordScore } = this.state;
    if (password === password2 && passwordScore) {
      this.props.resetPasswordConfirm(id, token, password);
      this.props.history.push("/login")
    }

  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.name === "password2" && this.state.password !== e.target.value) {
      this.setState({ validation: "Die Passwörter stimmen nicht überein" })
    }
    else this.setState({ validation: false })

  };

  handlePasswordScore = e => {
    this.setState({ passwordScore: e })
  }

  componentDidMount() {
    const { id, token } = this.props.match.params;
    this.setState({ id, token })
  }


  render() {
    const { error, loading, authenticated } = this.props;
    const { password, password2, validation } = this.state;

    if (authenticated) {
      return <Redirect to="/" />;
    }
    return (
      <div className={style.form}>
        <div className={`row d-flex vh-100 ${style.sideImage}`}>
          <div className={"col-md-6"}>
            <img className={"img-fluid"} src="/static/images/reset_password_bg.jpg" alt="" />
          </div>
          <div className={"col-md-6 py-4 pr-5 pl-5"}>
            <h2 className={style.mainHeading}>Gleich geschafft</h2>
            <div className={"row"}>
              <div className={"col-md-12"}>
                <form className={"mt-5"} action="" onSubmit={this.handleSubmit}>

                  <div className={style.formInput}>
                    <input
                      className={style.commonInput}
                      type="password"
                      onChange={this.handleChange}
                      value={password}
                      name="password"
                      placeholder="Passwort"
                      minLength="8"
                      required
                    />
                    <img
                      src="/static/images/key-solid.svg"
                      className={style.inputIcon}
                      alt=""
                    />
                    <PasswordStrengthBar password={password}
                      scoreWords={["schwaches Passwort", "schwaches Passwort", "da geht noch was", "gutes Passwort", "starkes Passwort"]}
                      shortScoreWord="zu kurzes Passwort"
                      minLength="8"
                      onChangeScore={this.handlePasswordScore}
                    />
                  </div>
                  <div className={style.formInput}>
                    <input
                      className={style.commonInput}
                      type="password"
                      onChange={this.handleChange}
                      value={password2}
                      name="password2"
                      minLength="8"
                      required
                      placeholder="Passwort wiederholen"
                    />
                    <img
                      src="/static/images/key-solid.svg"
                      className={style.inputIcon}
                      alt=""
                    />
                    <p>{validation}</p>
                  </div>
                  <div className="text-left mt-2">
                    <button type="submit" className={style.formBtn}>
                      <img src="/static/images/key-solidWhite.svg" alt="" className="mr-2" />
                        Neues Passwort setzen</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    authenticated: state.auth.token !== null
  };
};

const mapDispatchToProps = dispatch => {
  return {
    resetPasswordConfirm: (password, id, token) => dispatch(resetPasswordConfirm(password, id, token)),
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ResetPasswordConfirm));

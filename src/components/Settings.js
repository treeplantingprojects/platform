import React from "react";
import style from "../style/createArea.module.css";
import { Link, Redirect } from "react-router-dom";
import { updateProfile } from "../store/profile";
import { withRouter } from 'react-router-dom';



import { connect } from "react-redux";


class Settings extends React.Component {
  constructor(props) {
    super(props);

    this.inputReference = React.createRef();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    const { profile } = this.props
    this.state = {
      profile: null,
      error: null,
      loading: false,
      first_name: profile.first_name,
      last_name: profile.last_name,
      image: profile.image,
      email: profile.email,
      location: profile.location,
      profile_id: null
    };

  }

  fileUploadAction = () => this.inputReference.current.click();
  fileUploadInputChange = (e) => {
    this.setState({ fileUploadState: e.target.value });
    this.setState({ image: e.target.files[0] });
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    //const { email, first_name, last_name, location, image } = this.state;

    this.props.performUpdate(this.state, this.props.profile.profile_id);
    this.props.history.goBack();
  };


  render() {
    const { error, loading, token } = this.props;
    if (!token) {
      return <Redirect to="/" />;
    }
    if (loading) return ""
    return (
      <div className={style.form}>
        <div className={`row d-flex vh-100 ${style.sideImage}`}>
          <div className={"col-md-6"}>
            <img className={"img-fluid"} src="static/images/form-img.png" alt="" />
          </div>
          <div className={"col-md-6 py-4 pr-5 pl-5"}>
            <p onClick={this.props.history.goBack} className={style.backLink}>
              <img src="static/images/icon-arrow-left.svg" alt="" />
                zurück
              </p>
            <h2 className={style.mainHeading}>{this.state.first_name === "" ? (
              "Dein Profil"
            ) : (
              this.state.first_name + "'s Profil"
            )}</h2>
            <div className={"row"}>
              <div className={"col-md-10"}>
                <form className="mt-5" action="" onSubmit={this.handleSubmit}>
                  <div className={style.formInput}>
                    <input
                      className={style.commonInput}
                      type="text"
                      onChange={this.handleChange}
                      name="first_name"
                      value={this.state.first_name}
                      fluid="left"
                      required
                      placeholder="Vorname"
                    />
                    <img
                      className={style.inputIcon}
                      src="/static/images/users-Icon.svg"
                      alt=""
                    />
                  </div>
                  <div className={style.formInput}>
                    <input
                      className={style.commonInput}
                      type="text"
                      onChange={this.handleChange}
                      value={this.state.last_name}
                      name="last_name"
                      fluid="left"
                      required
                      placeholder="Nachname"
                    />
                    <img
                      className={style.inputIcon}
                      src="/static/images/users-Icon.svg"
                      alt=""
                    />
                  </div>
                  <div className={style.formInput}>
                    <input
                      readOnly="readOnly"
                      className={style.commonInput}
                      type="text"
                      required
                      placeholder={this.state.fileUploadState ? (this.state.fileUploadState.replace("C:\\fakepath\\", "")) : ("Bild hochladen")}
                      onClick={this.fileUploadAction}
                    />
                    <input
                      type="file"
                      style={{ display: "none" }}
                      name="image"
                      hidden
                      ref={this.inputReference}
                      onChange={this.fileUploadInputChange}
                    />
                    <img
                      className={style.inputIcon}
                      src="/static/images/image-Icon.svg"
                      alt=""
                    />
                  </div>
                  <div className={style.formInput}>
                    <input
                      className={style.commonInput}
                      type="text"
                      onChange={this.handleChange}
                      value={this.state.email}
                      name="email"
                      fluid="left"
                      required
                      placeholder="E-Mail"
                    />
                    <img
                      className={style.inputIcon}
                      src="/static/images/mail-Icon.svg"
                      alt=""
                    />
                  </div>
                  <div className={style.formInput}>
                    <input
                      className={style.commonInput}
                      type="text"
                      pattern="[0-9]{5}"
                      onChange={this.handleChange}
                      value={this.state.location}
                      name="location"
                      fluid="left"
                      required
                      placeholder="PLZ"
                    />
                    <img
                      className={style.inputIcon}
                      src="/static/images/addressIcon.svg"
                      alt=""
                    />
                  </div>
                  <div>
                    <button className={style.formBtn}>Speichern</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    performUpdate: (profile, id) => dispatch(updateProfile(profile, id))
  };
};

const mapStateToProps = state => {
  return {
    profile: state.profile,
    token: state.auth.token !== null
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Settings));

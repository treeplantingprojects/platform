import React, { useState } from "react";
import style from "../style/sidebar.module.css";
import { NavLink, Link } from "react-router-dom";
import { authLogout } from "../store/auth";
import { connect } from "react-redux";
import { fetchProfile } from "../store/profile";

class Sidebar extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    if (this.props.authenticated && !this.props.profile.profile) this.props.fetchProfile();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.authenticated && !this.props.profile.user_id && !this.props.profile.isLoading)
      this.props.fetchProfile();
  }

  toggleSidebar = () => {
    this.props.setSidebarVisibility(!this.props.setSidebarVisibility);
  }

  render() {
    const { authenticated, setSidebarVisibility, sidebarVisibility, version, profile } = this.props;
    return (
      <>
        <div
          className={style.Sidebar}
          style={{
            transform: sidebarVisibility ? "translate(0)" : "",
          }}
        >
          <div>
            <Link to={"/"} onClick={this.toggleSidebar}><h1>TreePlantingProjects</h1></Link>
            <div className={style.links}>
              <NavLink exact to="/" onClick={this.toggleSidebar}>
                <img src="/static/images/adminIcon.svg" alt="" /> Start
            </NavLink >
              <NavLink exact to="/projects" onClick={this.toggleSidebar}>
                <img src="/static/images/projectIcon.svg" alt="" />
              Projekte
            </NavLink >
              <NavLink exact to="/areas" onClick={this.toggleSidebar}>
                <img src="/static/images/areaIcon.svg" alt="" />
              Flächen
            </NavLink>
              <NavLink exact to="/blog" onClick={this.toggleSidebar}>
                <img src="/static/images/blogIcon.svg" alt="" />
              Blog
            </NavLink>
              <NavLink exact to="/search" onClick={this.toggleSidebar}>
                <img src="/static/images/sucheIcon.svg" alt="" />
              Suche
            </NavLink>
            </div>
          </div>
          {authenticated ? (
            <div className={style.user_profile}>
              <div>
                <img src={profile.image_url} alt="" className="img-fluid" />
              </div>
              <h5>{profile.first_name} {profile.last_name}</h5>
              <div className={style.user_profile}>
                <NavLink exact to="/settings" onClick={this.toggleSidebar}>
                  <p>Profileinstellungen</p>
                </NavLink>
              </div>
              <p className="mb-0" style={{ cursor: "pointer" }} onClick={() => {
                this.props.logout();
                this.toggleSidebar();
              }}>Logout</p>
              <p className={style.version_info}>Build: {version}</p>
            </div>
          ) : (
            <div className={style.user_profile}>
              <NavLink exact to="/login">
                <p style={{ cursor: "pointer" }} onClick={this.toggleSidebar}>Anmelden</p>
              </NavLink>
              <NavLink exact to="/signup">
                <p style={{ cursor: "pointer" }} onClick={this.toggleSidebar}>Registrieren</p>
              </NavLink>
              <p className={style.version_info}>Build: {version}</p>
            </div>
          )}

        </div>
        <button
          className={style.SidebarBtn}
          onClick={() => setSidebarVisibility(true)}
          style={{
            display: sidebarVisibility ? "none" : "",
          }}
        >
          <img src="/static/images/menu-Icon.svg" alt="" />
        </button>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.token !== null,
    version: state.version.name,
    profile: state.profile,
    setSidebarVisibility1: state.profile.setSidebarVisibility,
    sidebarVisibility1: state.profile.sidebarVisibility
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(authLogout()),
    fetchProfile: () => dispatch(fetchProfile())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar);

import React from "react";
import style from "../style/createArea.module.css";
import { Link, Redirect, withRouter } from "react-router-dom";
import PasswordStrengthBar from 'react-password-strength-bar';

import { connect } from "react-redux";
import { authSignup } from "../store/auth";
import GoogleLogin from "./GoogleLogin";


class SignUp extends React.Component {
  state = {
    email: "",
    password: "",
    password2: "",
    validation: false,
    passwordScore: false
  };

  handleSubmit = e => {
    e.preventDefault();
    const { email, password, password2, passwordScore } = this.state;
    if (password === password2 && passwordScore) this.props.signup(email, email, password, password2);

  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.name === "password2" && this.state.password !== e.target.value) {
      this.setState({ validation: "Die Passwörter stimmen nicht überein" })
    }
    else this.setState({ validation: false })

  };

  handlePasswordScore = e => {
    this.setState({ passwordScore: e })
  }

  render() {
    const { email, password, password2, validation } = this.state;
    const { error, loading, token, started } = this.props;
    if (token) {
      return <Redirect to="/" />;
    }
    return (
      <div className={style.form}>
        <div className={`row d-flex vh-100 ${style.sideImage}`}>
          <div className="col-md-6">
            <img className={"img-fluid"} src="static/images/signup_bg.jpg" alt="" />
          </div>
          <div className={"col-md-6 py-4 pr-5 pl-5"}>
            <p onClick={this.props.history.goBack} className={style.backLink}>
              <img src="static/images/icon-arrow-left.svg" alt="" />
                zurück
              </p>
            <h2 className={style.mainHeading}>Registrieren</h2>
            <div className={"row"}>
              <div className={"col-md-12"}>
                {!started ? (
                  <form className="mt-5" action="" onSubmit={this.handleSubmit}>
                    <div className={style.formInput}>
                      <input
                        className={style.commonInput}
                        type="email"
                        onChange={this.handleChange}
                        value={email}
                        name="email"
                        required
                        placeholder="E-Mail-Adresse"
                      />
                      <img
                        src="static/images/users-Icon.svg"
                        className={style.inputIcon}
                        alt=""
                      />
                    </div>
                    <div className={style.formInput}>
                      <input
                        className={style.commonInput}
                        type="password"
                        onChange={this.handleChange}
                        value={password}
                        name="password"
                        placeholder="Passwort"
                        minLength="8"
                        required
                      />
                      <img
                        src="static/images/key-solid.svg"
                        className={style.inputIcon}
                        alt=""
                      />
                      <PasswordStrengthBar password={password}
                        scoreWords={["schwaches Passwort", "schwaches Passwort", "da geht noch was", "gutes Passwort", "starkes Passwort"]}
                        shortScoreWord="zu kurzes Passwort"
                        minLength="8"
                        onChangeScore={this.handlePasswordScore}
                      />
                    </div>
                    <div className={style.formInput}>
                      <input
                        className={style.commonInput}
                        type="password"
                        onChange={this.handleChange}
                        value={password2}
                        name="password2"
                        minLength="8"
                        required
                        placeholder="Passwort wiederholen"
                      />
                      <img
                        src="static/images/key-solid.svg"
                        className={style.inputIcon}
                        alt=""
                      />
                      <p>{validation}</p>
                    </div>
                    <div className="text-left mt-2 row">
                      <div className={"col-md-8"}>
                        <button className={style.formBtn}>
                          <img src="/static/images/sign-in-alt-solid.svg" alt="" className="mr-2" />
                            Los geht's
                          </button>
                      </div>
                      <div className={`col-md-4 ${style.socialLogin}`}>
                        <GoogleLogin />
                      </div>
                    </div>

                  </form>
                ) : (
                  <p>Bitte bestätige deine E-Mail.</p>
                )}
                <div className={`${style.mainText} row`}>
                  <Link to={"/login"}>
                    <p>Bereits registriert?</p>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    token: state.auth.token,
    started: state.auth.started
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signup: (username, email, password1, password2) =>
      dispatch(authSignup(username, email, password1, password2))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SignUp));

import React from "react";
import style from "../style/smallCard.module.css";
export default function SmallCard() {
  return (
    <div className={style.SmallCard}>
      <img src="/images/smCardImage.svg" alt="" />
      <div className={style.tooltip}>Organisator</div>
      <img src="/images/Avatar-2.png" alt="" />
      <div>
        <h6>Sergei Filatov</h6>
        <p>seit 15.02.20</p>
      </div>
    </div>
  );
}

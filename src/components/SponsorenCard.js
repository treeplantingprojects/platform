import React from "react";
import style from "../style/SponsorenCard.module.css";

export default function SponsorenCard({ data }) {
  const { image, icon, title, description } = data;
  return (
    <div className={`${style.SponsorenCard} mb-3 mb-md-0`}>
      {image ? <img src={`/images/${image}`} alt="" /> : ""}
      <div className={style.cardContent}>
        {icon ? <img src={`/images/${icon}`} alt="" /> : ""}
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

import React, { useState } from "react";
import style from "../style/Admin.module.css";

export default function TableRow({ setEditVisible, Data, editVisible }) {
  const [name, setName] = useState("Jonas Schmi|");
  const handleChange = (e) => {
    if (e.which == 13) {
      setName(e.target.value);
      setEditVisible("");
    }
  };
  return (
    <tr className={editVisible == Data.id ? style.active : ""}>
      <td>
        <label className={style.containerCheckbox}>
          <input type="checkbox" />
          <span className={style.checkmark}></span>
        </label>
      </td>
      <td className="pl-2">
        <img src="/images/image.png" alt="" />
      </td>
      <td>
        {editVisible == Data.id ? (
          <input type="text" onKeyDown={handleChange} autoFocus />
        ) : (
          <span onClick={() => setEditVisible(Data.id)}>{Data.name}</span>
        )}
      </td>
      <td>{Data.email}</td>
      <td>
        <div class="dropdown">
          <div
            class="dropdown-toggle"
            type="button"
            id="dropdownMenuButton"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Admin
          </div>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">
              Action
            </a>
            <a class="dropdown-item" href="#">
              Another action
            </a>
            <a class="dropdown-item" href="#">
              Something else here
            </a>
          </div>
        </div>
      </td>
      <td>{Data.project}</td>
      <td>{Data.Zukunftsbäume}</td>
      <td>{Data.Ort}</td>
      <td>{Data.Letzte}</td>
      <td>
        <div className="d-flex align-items-center">
          {Data.Mailings == 1 ? (
            <div className={style.circle}></div>
          ) : Data.Mailings == 2 ? (
            <>
              <div className={style.circle}></div>
              <div className={`${style.circle} mx-2`}></div>
            </>
          ) : Data.Mailings == 3 ? (
            <>
              <div className={style.circle}></div>
              <div className={`${style.circle} mx-2`}></div>
              <div className={style.circle}></div>
            </>
          ) : (
            ""
          )}
        </div>
      </td>
      {editVisible == Data.id ? (
        <td style={{backgroundColor: "#ffffff"}}>
          <button className={style.ResetBtn}>
            <img src="/images/undo-Icon.svg" alt="" />
          </button>
        </td>
      ) : (
        ""
      )}
    </tr>
  );
}

import React from "react";
import style from "../style/projects.module.css";
import ProjectCard from "../components/ProjectCard";
import AreaCard from "../components/AreaCard";
import EventCard from "../components/EventCard";
import { Link } from "react-router-dom";

export default function YourAreas({ data, areas }) {
  const { title, button1, button1Icon, button2, button2Icon } = areas;
  return (
    <div className={style.yourProject}>
      <div className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-between mb-3">
        <h1 className="mr-3 mb-3 mb-sm-0">Deine Flächen</h1>
        <div>
          <Link to={"/report_area"}>
            <button className="primaryBtn mr-2">
              <img src="/static/images/plus-square-Icon.svg" alt="" className="mr-2" />{" "}
                  Fläche melden
                </button>
          </Link>
        </div>
      </div>
      <div
        className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-start"
        style={{ marginLeft: "-1em" }}
      >

      </div>
    </div>
  );
}

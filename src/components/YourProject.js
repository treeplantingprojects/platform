import React from "react";
import style from "../style/projects.module.css";
import ProjectCard from "../components/ProjectCard";
import AreaCard from "../components/AreaCard";
import EventCard from "../components/EventCard";
import { Link } from "react-router-dom";

export default function YourProject({ data, yourProject }) {
  const { title, button1, button1Icon, button2, button2Icon } = yourProject;
  return (
    <div className={style.yourProject}>
      <div className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-between mb-3">
        <h1 className="mr-3 mb-3 mb-sm-0">{title}</h1>
        <div>
          {button2 ? (
            <button className="primaryBtn mr-2">
              <img src={`/images/${button2Icon}`} alt="" className="mr-2" />{" "}
              {button2}
            </button>
          ) : (
            ""
          )}
          <Link
            to={`${
              title == "Your areas"
                ? "/create_area"
                : title == "Your events"
                ? "/event/event_detail"
                : "/project"
            }`}
          >
            <button className="primaryBtn">
              <img src={`/images/${button1Icon}`} alt="" className="mr-2" />{" "}
              {button1}
            </button>
          </Link>
        </div>
      </div>
      <div
        className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-start"
        style={{ marginLeft: "-1em" }}
      >
        {title == "Your areas" ? (
          <>
            <AreaCard data={data} />
            <AreaCard data={data} />
            <AreaCard data={data} />
          </>
        ) : title == "Your events" ? (
          <>
            <EventCard data={data} />
            <EventCard data={data} />
            <EventCard data={data} />
          </>
        ) : (
          <>
            <Link to={`/project/project_detail`}>
              <ProjectCard data={data} />
            </Link>
            <Link to={`/project/project_detail`}>
              <ProjectCard data={data} />
            </Link>
            <Link to={`/project/project_detail`}>
              <ProjectCard data={data} />
            </Link>
          </>
        )}
      </div>
    </div>
  );
}

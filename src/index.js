import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import api from "./store/middleware/api";
import cleanup from "./store/middleware/cleanup";
import { persistStore, persistReducer } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';

import storageSession from 'redux-persist/es/storage/session';

import versionReducer from "./store/version";
import authReducer from "./store/auth";
import profileReducer from "./store/profile";
import projectsReducer from "./store/projects";
import areasReducer from "./store/areas";
import { fetchVersion, versionCheckState } from "./store/version";
import blogReducer from "./store/blog";
import { checkAuthTimeout } from "./store/auth";

const persistConfig = {
  key: 'root',
  storage: storageSession,
}

const rootReducer = combineReducers({
  version: versionReducer,
  auth: authReducer,
  profile: profileReducer,
  projects: projectsReducer,
  areas: areasReducer,
  blog: blogReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
  reducer: persistedReducer,
  middleware: [api, cleanup, thunk]
});

const verionSubscriber = store.subscribe(() => {
  if(store.getState().version.name) {
    store.dispatch(versionCheckState())
    verionSubscriber();
  }
});

store.dispatch(fetchVersion())

const authSubscriber = store.subscribe(() => {
  if(store.getState().auth.token) {
    store.dispatch(checkAuthTimeout(3600));
    authSubscriber();
  }
});

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistStore((store))}>
        <App />
      </PersistGate>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

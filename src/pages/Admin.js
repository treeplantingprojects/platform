import React, { useState } from "react";
import style from "../style/Admin.module.css";
import { Input } from "antd";
import TableRow from "../components/TableRow";
import AdminChartsSection from "../components/AdminChartsSection";

export default function Admin() {
  const [editVisible, setEditVisible] = useState();
  const [tableRowData, setTableRowData] = useState([
    {
      id: 1,
      name: "Jonas Schmi|",
      email: "jonas.schmidt@gmail.com",
      project: "26",
      Zukunftsbäume: "6",
      Ort: "Berlin",
      Letzte: "01.01.2021",
      Mailings: "3",
    },
    {
      id: 2,
      name: "Jonas Schmi|",
      email: "jonas.schmidt@gmail.com",
      project: "26",
      Zukunftsbäume: "6",
      Ort: "Berlin",
      Letzte: "01.01.2021",
      Mailings: "1",
    },
    {
      id: 3,
      name: "Jonas Schmi|",
      email: "jonas.schmidt@gmail.com",
      project: "26",
      Zukunftsbäume: "6",
      Ort: "Berlin",
      Letzte: "01.01.2021",
      Mailings: "2",
    },
  ]);
  return (
    <div className={style.Admin}>
      <AdminChartsSection />
      <div className={style.AdminTable}>
        <div
          className={`${style.header} d-flex align-items-center justify-content-between`}
        >
          <Input
            placeholder="Search"
            prefix={
              <img
                src="/images/searchIcon.svg"
                alt=""
                style={{ width: "1.275625em" }}
              />
            }
          />
          <div>
            <button>Deaktivieren</button>
            <button>Passwort zurücksetzen</button>
            <button>Speichern</button>
          </div>
        </div>
        <table>
          <thead>
            <tr>
              <th>
                <label className={style.containerCheckbox}>
                  <input type="checkbox" />
                  <span className={style.checkmark}></span>
                </label>
              </th>
              <th></th>
              <th>Name</th>
              <th>Email</th>
              <th>Rolle</th>
              <th>Projekte</th>
              <th>Zukunftsbäume</th>
              <th>Ort</th>
              <th>Letzte Anmeldung</th>
              <th>Mailings</th>
            </tr>
          </thead>
          <tbody>
            {tableRowData.map((item) => (
              <TableRow
                editVisible={editVisible}
                Data={item}
                setEditVisible={(item) => setEditVisible(item)}
              />
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

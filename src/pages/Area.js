import React, { useState } from "react";
import AreasGoal from "../components/AreasGoal";
import YourAreas from "../components/YourAreas";
import style from "../style/projects.module.css";
import goalStyle from "../style/goal.module.css";
import { connect } from "react-redux";
import data from "../data/areaData.json";
import { fetchProjects, fetchSubProjects } from "../store/projects";
import { fetchAreas } from "../store/areas";
import { Link } from "react-router-dom";
import ProjectCard from "../components/ProjectCard";
import AreaCard from "../components/AreaCard";

class Area extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.authenticated) this.props.getAreas();
    }
    render() {
        const { authenticated, areas, isLoaded } = this.props;
        return (
            <div className={style.Projects}>
                {authenticated && isLoaded ? (
                    <div className={style.yourProject}>
                        <div className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-between mb-3">
                            <h1 className="mr-3 mb-3 mb-sm-0">Deine Flächen</h1>
                            <Link to={"/report_area"}>
                                <button className="primaryBtn mr-2">
                                    <img src={`static/images/plus-square-Icon.svg`} alt="" className="mr-2" />{" "}
                              Fläche melden
                            </button>

                            </Link>
                        </div>
                        <div
                            className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-start"
                            style={{ marginLeft: "-1em" }}
                        >
                            {Object.keys(areas).map((id => (
                                <div key={id}>
                                    <AreaCard key={id} data={areas[id]} />
                                </div>
                            )))
                            }
                        </div>
                    </div>
                ) : (
                    <AreasGoal />
                )}
                {/*<div>*/}
                {/*  <h1 className="my-5">Weitere Informationen</h1>*/}
                {/*  <div*/}
                {/*      className="d-flex align-items-center"*/}
                {/*      style={{marginTop: "2em"}}*/}
                {/*  >*/}
                {/*      <p>In unserer Wissensdatenbank...</p>*/}

                {/*  </div>*/}
                {/*</div>*/}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        authenticated: state.auth.token !== null,
        areas: state.areas.list,
        isLoaded: state.areas.isLoaded,
        error: state.projects.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getAreas: () => dispatch(fetchAreas()),
    };
};


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Area);

import React, { useState } from "react";
import style from "../style/blog.module.css";
import BlogGoal from "../components/BlogGoal";
import ProjectCard from "../components/ProjectCard";
import data from "../data/blogData.json";
import { connect } from "react-redux";
import { fetchBlog } from "../store/blog";
import BlogCard from "../components/BlogCard";

class Blog extends React.Component {
    constructor(props) {
        super(props);
        this.postsToLoad = 6;
    }

    componentDidMount() {
        this.props.getBlog(this.postsToLoad)
    }

    loadMorePosts = () => {
        this.postsToLoad += 5
        this.props.getBlog(this.postsToLoad);
    }

    render() {
        const { posts, loaded, error } = this.props;
        if (error) {
            return "";
        } else {
            const mainPost = posts[0]
            let cardPosts = posts.slice(1)
            return (
                <div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className={style.Goal}>
                                <BlogGoal post={mainPost} />
                            </div>
                        </div>
                    </div>
                    <div className={style.Blog}>
                        <div
                            className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-start"
                            style={{ marginBottom: "7em", marginLeft: "-1em" }}
                        >
                            {cardPosts.map((post => (
                                <a
                                    href={post.link} key={post.title} target="_blank"
                                >
                                    <BlogCard post={post} />
                                </a>
                            )))}
                            <button className="primaryBtn mr-2 ml-3" style={{ padding: "1.2em 3.1em" }} onClick={this.loadMorePosts}>
                                <img src={`static/images/menu-Icon.svg`} className="mr-2" />
                            Weitere laden
                        </button>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

const mapStateToProps = state => {
    return {
        authenticated: state.auth.token !== null,
        posts: state.blog.list,
        loaded: state.blog.loaded,
        error: state.blog.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getBlog: (limit) => dispatch(fetchBlog(limit))
    };
};


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Blog);

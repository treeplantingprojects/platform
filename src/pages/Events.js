import React from "react";
import YourProject from "../components/YourProject";
import style from "../style/projects.module.css";
import EventCard from "../components/EventCard";
import data from "../data/events.json";

export default function Events() {
  return (
    <div className={style.Projects}>
      <YourProject data={data[0]} yourProject={data[1]} />
      <div>
        <h1 className="my-5">Events</h1>
        <img src="/images/areaMap.png" alt="" className="img-fluid" />
        <div
          className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-start"
          style={{ marginLeft: "-1em", marginTop: "1em" }}
        >
          <EventCard data={data[0]} />
          <EventCard data={data[0]} />
          <EventCard data={data[0]} />
          <EventCard data={data[0]} />
          <EventCard data={data[0]} />
          <EventCard data={data[0]} />
        </div>
      </div>
    </div>
  );
}

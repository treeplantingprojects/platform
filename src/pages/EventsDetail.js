import React, { useState } from "react";
import Header from "../components/Header";
import style from "../style/event.module.css";
import EventGoal from "../components/EventGoal";
import Information from "../components/Information";
import SponsorenCard from "../components/SponsorenCard";
import EventCard from "../components/EventCard";
import data from "../data/eventData.json";

export default function Events() {
  return (
    <div className={style.Events}>
      <Header data={data[0]} />
      <EventGoal />

      <div style={{ marginBottom: "7em" }}>
        <Information />
      </div>
      <div
        className="d-flex align-items-center"
        style={{ marginBottom: "2.5em" }}
      >
        <h1>Sponsoren</h1>
      </div>
      <div
        className="d-flex align-items-end flex-wrap"
        style={{ marginLeft: "-1.5em", marginBottom: "7em" }}
      >
        {data[1].sponCradData.map((data) => (
          <SponsorenCard data={data} />
        ))}
      </div>
      <div
        className="d-flex align-items-center"
        style={{ marginBottom: "2.5em" }}
      >
        <h1>News</h1>
      </div>
      <div
        className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-start"
        style={{ marginBottom: "7em", marginLeft: "-1em" }}
      >
        <EventCard data={data[2]} />
        <EventCard data={data[2]} />
        <EventCard data={data[2]} />
      </div>
    </div>
  );
}

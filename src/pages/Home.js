import React, { useState } from "react";
import HomeGoal from "../components/HomeGoal";
import style from "../style/home.module.css";
import {connect} from "react-redux";
import {fetchBlog} from "../store/blog";
import BlogCard from "../components/BlogCard";
import {Link} from "react-router-dom";

class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getBlog()
    }

    render() {
        const {posts, loaded, error, authenticated} = this.props;
        if (!loaded) {
            return "";
        } else {
            return (
                <div className={style.ProjectDetail}>
                    <HomeGoal authenticated={authenticated}/>
                    <div
                        className="d-flex align-items-center"
                        style={{marginBottom: "2.5em"}}
                    >
                        <h1>Neuigkeiten</h1>
                        <Link to="/blog">Alle Blogeinträge anzeigen</Link>
                    </div>
                    <div
                        className="d-flex align-items-end flex-wrap"
                        style={{marginLeft: "-1.5em", marginBottom: "2.5em"}}
                    >
                        {posts.map((post => (
                                     <a
                                            href={post.link} key={post.title} target="_blank"
                                        >
                                        <BlogCard post={post}/>
                                     </a>
                            )))}
                    </div>
                </div>
            );
        }
    }
}

const mapStateToProps = state => {
    return {
        authenticated: state.auth.token !== null,
        posts: [state.blog.list[0], state.blog.list[1]],
        loaded: state.blog.loaded,
        error: state.blog.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getBlog: () => dispatch(fetchBlog())
    };
};


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);

import React, { useState } from "react";
//import ProjectAreas from "../components/ProjectAreas";
import ProjectGoal from "../components/ProjectGoal";
import style from "../style/projectDeatil.module.css";
import ProjectAreas from "../components/ProjectAreas";
import ProjectMembers from "../components/ProjectMembers";
import Information from "../components/Information";
import Header from "../components/Header";
import data from "../data/projectDetail.json";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import {
  fetchProjectDetails,
  fetchProjectDetailsData,
  resetProjectDetailsLoad,
} from "../store/projects";
import ProjectNews from "../components/ProjectNews";


class ProjectDetail extends React.Component {
  constructor(props) {
    super(props);
    this.project_id = this.props.match.params.projectName
  }

  componentDidMount() {
    this.props.getProjectDetails(this.project_id, this.props.authenticated)
  }

  componentWillUnmount() {
    this.props.resetProjectDetailsLoad()
  }


  render() {
    const { loading, loaded, imagesLoaded, areasLoaded, subscribersLoaded, authenticated, membersLoaded, newsLoaded } = this.props;
    if (loading || !loaded || !imagesLoaded || !areasLoaded || !subscribersLoaded || !membersLoaded || !newsLoaded) {
      return "";
    } else {
      const projectDetails = this.props.projectDetails[this.project_id];
      return (
        <div className={style.ProjectDetail}>
          <Header data={data[0]} />
          <ProjectGoal id={"Übersicht"} data={projectDetails} has_sub={this.props.subState(this.project_id)} authenticated={authenticated} />
          <div
            className="d-flex align-items-center"
            style={{ marginBottom: "2.5em" }}
            id={"Mitglieder"}
          >
            <h1>Unterstützer*innen</h1>
            {authenticated ? (
              <Link to="/projects_join">Ich will auch mitmachen</Link>
            ) : (
              <Link to="/login">Melde dich an um ein Projekt Mitglie zu werden</Link>
            )}

          </div>
          <ProjectMembers members={projectDetails.members} />
          {projectDetails.news.length ? (
            <div>
              <div
                className="d-flex align-items-center pt-5"
                style={{ marginBottom: "2.5em" }}
                id={"Neuigkeiten"}
              >
                <h1>Neuigkeiten</h1>
              </div>
              <div className={style.Projects}>
                <ProjectNews news={projectDetails.news} />
              </div>
            </div>
          ) : ("")}
          {projectDetails.areas.length ? (
            <div>
              <div
                className="d-flex align-items-center pt-5"
                style={{ marginBottom: "2.5em" }}
                id={"Flächen"}
              >
                <h1>Flächen</h1>
              </div>
              <div className={style.Projects}>
                <ProjectAreas areas={projectDetails.areas} />
              </div>
            </div>
          ) : ("")}

          <div
            className="d-flex align-items-center pt-5"
            style={{ marginBottom: "2.5em" }}
            id={"Informationen"}
          >
            <h1>Informationen</h1>
          </div>
          <Information data={projectDetails} />
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    projectDetails: state.projects.details,
    authenticated: state.auth.token !== null,
    loading: state.projects.detailsIsLoading,
    loaded: state.projects.detailsLoaded,
    imagesLoaded: state.projects.imagesLoaded,
    areasLoaded: state.projects.areasLoaded,
    subscribersLoaded: state.projects.subscribersLoaded,
    membersLoaded: state.projects.membersLoaded,
    newsLoaded: state.projects.newsLoaded,
    subState: (projectId) => !!state.projects.subscribed[projectId],

  };
};

const mapDispatchToProps = dispatch => {
  return {
    getProjectDetails: (projectId, auth) => dispatch(fetchProjectDetails(projectId, auth)),
    getProjectDetailsData: (projectId, auth) => dispatch(fetchProjectDetailsData(projectId, auth)),
    resetProjectDetailsLoad: () => dispatch(resetProjectDetailsLoad()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectDetail);

import React, { useState } from "react";
import style from "../style/projects.module.css";
import ProjectsGoal from "../components/ProjectsGoal";
import ProjectCard from "../components/ProjectCard";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchProjects, fetchSubProjects } from "../store/projects";



class Projects extends React.Component {
    constructor(props) {
        super(props);
        this.projectsToLoad = 5;
    }

    componentDidMount() {
        this.props.getProjects(this.projectsToLoad);
        if (this.props.authenticated) this.props.getSubProjects();
    }

    loadMoreProjects = () => {
        this.projectsToLoad += 5
        this.props.getProjects(this.projectsToLoad);
    }

    render() {
        const { authenticated, projects, subProjects, isLoading, subIsLoaded, error } = this.props
        if (error) {
            return "";
        }
        return (
            <div>
                <div className={style.Projects}>
                    {authenticated && subIsLoaded && Object.keys(subProjects).length !== 0 ? (
                        <div className={style.yourProject}>
                            <div className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-between mb-3">
                                <h1 className="mr-3 mb-3 mb-sm-0">Deine Projekte</h1>
                                <Link to={"/search"}>
                                    <button className="primaryBtn mr-2" style={{ padding: "1.2em 3.1em" }}>
                                        <img src="/static/images/searchIcon.svg" className="mr-2" />
                              Projekte suchen
                            </button>

                                </Link>
                            </div>
                            <div
                                className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-start"
                                style={{ marginLeft: "-1em" }}
                            >
                                {Object.keys(subProjects).map((id => (
                                    <div key={id}>
                                        <Link
                                            key={id} to={`/projects/${id}`}
                                        >
                                            <ProjectCard key={id} data={subProjects[id]} />
                                        </Link>
                                    </div>
                                )))
                                }
                            </div>
                        </div>
                    ) : (
                        <ProjectsGoal authenticated={authenticated} />
                    )}
                    <div>
                        <h1 className="my-5">Aktuelle Projekte</h1>
                        <div
                            className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-start"
                            style={{ padding: "0em 1.5em", marginLeft: "-1em" }}
                        >
                            {projects.map((project => (
                                <div key={project.id} variants={this.item}>
                                    <Link
                                        to={`/projects/${project.id}`} key={project.id}
                                    >
                                        <ProjectCard key={project.id} data={project} />
                                    </Link>
                                </div>
                            )))}
                            <button className="primaryBtn mr-2 ml-3" style={{ padding: "1.2em 3.1em" }} onClick={this.loadMoreProjects}>
                                <img src={`static/images/menu-Icon.svg`} className="mr-2" />
                      Weitere laden
                </button>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        authenticated: state.auth.token !== null,
        projects: state.projects.list,
        subProjects: state.projects.subscribed,
        subIsLoaded: state.projects.subIsLoaded,
        isLoading: state.projects.isLoading,
        error: state.projects.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getProjects: (n) => dispatch(fetchProjects(n)),
        getSubProjects: () => dispatch(fetchSubProjects())
    };
};


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Projects);

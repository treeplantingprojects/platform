import React from "react";
import ProjectCard from "../components/ProjectCard";
import style from "../style/suche.module.css";
import algoliasearch from 'algoliasearch/lite';
import { InstantSearch, connectHits, connectSearchBox } from 'react-instantsearch-dom';
import {
  GoogleMapsLoader,
  GeoSearch,
  Marker,
} from 'react-instantsearch-dom-maps';
import { motion } from "framer-motion";
import { Input } from "antd";
import { Link } from "react-router-dom";


class Search extends React.Component {
  constructor(props) {
    super(props)

    this.searchClient = algoliasearch(
      process.env.ALGOLIA_APP_ID,
      process.env.ALGOLIA_API_KEY
    );
  }

  render() {
    return (
      <InstantSearch indexName={`${process.env.STAGE}_Project`} searchClient={this.searchClient}>
        <div className={style.Suche}>
          <div
            className={`${style.header} justify-content-center justify-content-md-start`}
          >
            <Searchbox />
          </div>
          <h1>Projekte</h1>
          <Projects />
          {/*<h1>Maps</h1>*/}

        </div>
      </InstantSearch>
    )
  }
}


const SearchComponent = ({ currentRefinement, isSearchStalled, refine }) => (
  <form noValidate action="" role="search" style={{ display: "contents" }}>
    <Input
      placeholder="Suche nach Projekten (Name/PLZ/Region)"
      style={{ backgroundColor: "#ffffff" }}
      className={`mb-2 mb-md-0`}
      value={currentRefinement}
      onChange={event => refine(event.currentTarget.value)}
      prefix={
        <img
          src="/static/images/searchIconGrey.svg"
          alt=""
          style={{ width: "1.275625em" }}
        />
      }
    />
    {isSearchStalled ? 'My search is stalled' : ''}
  </form>
);

const Hits = ({ hits }) => (
  <div
    className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-start"
    style={{ marginLeft: "-1em" }}
  >
    {hits.map(hit => (
      <div key={hit.objectId} >
        <Link to={`/projects/${hit.id}`} key={hit.id} >
          <ProjectCard key={hit.objectID} data={{ ...hit, image: hit.image_url.split("?AWS")[0] }} />
        </Link>
      </div>
    ))}
  </div>

);

const Projects = connectHits(Hits);
const Searchbox = connectSearchBox(SearchComponent);

export default Search;
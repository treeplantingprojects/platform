import React from "react";
import ProjectCard from "../components/ProjectCard";
import AreaCard from "../components/AreaCard";
import EventCard from "../components/EventCard";
import data from "../data/sucheData.json";
import style from "../style/suche.module.css";
import { Input } from "antd";

export default function Suche() {
  return (
    <div className={style.Suche}>
      <div
        className={`${style.header} justify-content-center justify-content-md-start`}
      >
        <Input
          placeholder="Search"
          style={{ backgroundColor: "#ffffff" }}
          className={`mb-2 mb-md-0`}
          prefix={
            <img
              src="/images/searchIcon.svg"
              alt=""
              style={{ width: "1.275625em" }}
            />
          }
        />
        <div className={`${style.filter_by} mb-2 mb-md-0`}>
          <p>Filter by:</p>
          <div>
            <span>thisisafilter</span>
            <span>anotherfilter</span>
            <span>anotherone</span>
          </div>
        </div>
        <div className={`${style.pageLinks} mb-2 mb-md-0`}>
          <span>Projects</span>
          <span>Areas</span>
          <span>Events</span>
        </div>
        <div className={`${style.changeLayoutBtn} mb-2 mb-md-0`}>
          <button>
            <img src="/images/list.svg" alt="" />
          </button>
          <button>
            <img src="/images/grid.svg" alt="" />
          </button>
        </div>
      </div>
      <h1>Projects</h1>
      <div
        className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-start"
        style={{ marginLeft: "-1em" }}
      >
        <ProjectCard data={data[0]} />
        <ProjectCard data={data[0]} />
        <ProjectCard data={data[0]} />
      </div>
      <h1>Areas</h1>
      <div
        className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-start"
        style={{ marginLeft: "-1em" }}
      >
        <AreaCard data={data[1]} />
        <AreaCard data={data[1]} />
        <AreaCard data={data[1]} />
      </div>
      <h1>Events</h1>
      <div
        className="d-flex align-items-center flex-wrap justify-content-center justify-content-sm-start"
        style={{ marginLeft: "-1em" }}
      >
        <EventCard data={data[2]} />
        <EventCard data={data[2]} />
        <EventCard data={data[2]} />
      </div>
    </div>
  );
}

import React, { useState } from "react";
import Footer from "../../components/Footer";
import Sidebar from "../../components/Sidebar";
import { AiOutlineClose } from "react-icons/ai";
import "../../App.css";

export default function DashboardFooterLayout({ children }) {
  const [sidebarVisibility, setSidebarVisibility] = useState(false);
  return (
    <div
      style={{
        overflow: sidebarVisibility ? "hidden" : "",
        height: sidebarVisibility ? "100vh" : "",
      }}
    >
      <div className="d-flex">
        <Sidebar
          sidebarVisibility={sidebarVisibility}
          setSidebarVisibility={(item) => setSidebarVisibility(item)}
        />
        <div className="px-2 px-md-4 py-2 py-md-3 content">
          {sidebarVisibility ? (
            <div
              style={{
                backgroundColor: "#262626",
                position: "fixed",
                inset: "0px",
                zIndex: "4",
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-end",
              }}
            >
              <button
                onClick={() => setSidebarVisibility(false)}
                style={{
                  backgroundColor: "#ffffff",
                  width: "30px",
                  height: "30px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: "50%",
                  marginRight: "2em",
                  border: "none",
                }}
              >
                <AiOutlineClose />
              </button>
            </div>
          ) : (
            ""
          )}

          {children}
        </div>
      </div>
      <Footer />
    </div>
  );
}

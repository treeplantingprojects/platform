import React, { useState } from "react";
import Sidebar from "../../components/Sidebar";
import { AiOutlineClose } from "react-icons/ai";
import "../../App.css";

export default function DashboardLayout({ children }) {
  const [sidebarVisibility, setSidebarVisibility] = useState(false);
  return (
    <div
      className="d-flex"
      style={{
        overflow: "hidden",
        height: "100vh",
      }}
    >
      <Sidebar
        sidebarVisibility={sidebarVisibility}
        setSidebarVisibility={(item) => setSidebarVisibility(item)}
      />
      <div className="px-4 py-3 content">
        {sidebarVisibility ? (
          <div
            style={{
              backgroundColor: "#262626",
              position: "fixed",
              inset: "0px",
              zIndex: "4",
              width: "100vw",
              height: "100vh",
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-end",
            }}
          >
            <button
              onClick={() => setSidebarVisibility(false)}
              style={{
                backgroundColor: "#ffffff",
                width: "30px",
                height: "30px",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                borderRadius: "50%",
                marginRight: "2em",
                border: "none",
              }}
            >
              <AiOutlineClose />
            </button>
          </div>
        ) : (
          ""
        )}

        {children}
      </div>
    </div>
  );
}

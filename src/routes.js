import React, { Component } from "react";
import { Switch, Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";

import Home from "./pages/Home";
import Project from "./pages/Projects";
import Area from "./pages/Area";
import Blog from "./pages/Blog.js";
import CreateArea from "./components/CreateArea";
import ProjectDetail from "./pages/ProjectDetail";
import Suche from "./pages/Suche";
import Search from "./pages/Search";
import Login from "./components/Login";

import DashboardLayout from "./pages/layouts/DashboardLayout";
import PlainLayout from "./pages/layouts/PlainLayout";
import DashboardFooterLayout from "./pages/layouts/DashboardFooterLayout";
import Signup from "./components/SignUp";
import ReportArea from "./components/ReportArea";
import Settings from "./components/Settings";
import ProjectsJoin from "./components/ProjectsJoin";
import EmailVerify from "./components/EmailVerify";
import Maintenance from "./components/Maintenance";
import ResetPassword from "./components/ResetPassword";
import ResetPasswordConfirm from "./components/ResetPasswordConfirm";

class Routes extends Component {
  constructor(props) {
    super(props)
  }
  componentDidUpdate() {
    if (this.props.maintenance && this.props.location.pathname !== "/maintenance") this.props.history.push("/maintenance")
  }

  render() {
    return (
      <Switch>
        <Route
          exact
          path={["/", "/projects", "/areas", "/blog", "/search", "/event", "/projects/:projectName"]}
        >
          <DashboardLayout>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/projects" component={Project} />
              <Route exact path="/projects/:projectName" component={ProjectDetail} />
              <Route exact path="/areas" component={Area} />
              <Route exact path="/blog" component={Blog} />
              <Route exact path="/search" component={Search} />
            </Switch>
          </DashboardLayout>
        </Route>
        <Route exact path={["/create_area", "/report_area"]}>
          <PlainLayout>
            <Switch>
              <Route exact path="/create_area" component={CreateArea} />
            </Switch>
            <Switch>
              <Route exact path="/report_area" component={ReportArea} />
            </Switch>
          </PlainLayout>
        </Route>
        <Route exact path={[
          "/login", "/signup", "/settings",
          "/projects_join", "/email_verify/:id/:token",
          "/maintenance", "/reset_password", "/reset_password/confirm/:id/:token"
        ]}>
          <PlainLayout>
            <Switch>
              <Route exact path="/login" component={Login} />
            </Switch>
            <Switch>
              <Route exact path="/signup" component={Signup} />
            </Switch>
            <Switch>
              <Route exact path="/settings" component={Settings} />
            </Switch>
            <Switch>
              <Route exact path="/projects_join" component={ProjectsJoin} />
            </Switch>
            <Switch>
              <Route exact path="/email_verify/:id/:token" component={EmailVerify} />
            </Switch>
            <Switch>
              <Route exact path="/maintenance" component={Maintenance} />
            </Switch>
            <Switch>
              <Route exact path="/reset_password" component={ResetPassword} />
            </Switch>
            <Switch>
              <Route exact path="/reset_password/confirm/:id/:token" component={ResetPasswordConfirm} />
            </Switch>
          </PlainLayout>
        </Route>
      </Switch>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    maintenance: state.version.maintenance,
  };
};


export default withRouter(
  connect(
    mapStateToProps,
    null
  )(Routes));

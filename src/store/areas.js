import { createSlice } from "@reduxjs/toolkit";
import {authFail, authStart, authSuccess} from "./auth";


const slice = createSlice({
    name: 'areas',
    initialState: {
        isLoaded: false,
        error: null,
        list: []
    },
    reducers: {
        areasFetchLoading: (areas, action) => {
            areas.isLoaded = false;
            areas.error = null;
        },
        areasFetchSuccess: (areas, action) => {
            areas.list = action.payload.results
            areas.isLoaded = true;
        },
        areasFetchFail: (areas, action) => {
            areas.isLoaded = true;
            areas.error = action.payload;
        },
        areasLogout: (areas) => {
            areas = []
        },
        reportAreaSuccess: (areas, action) => {
            areas.list.push(action.payload);
        }
    }
});

export const { areasFetchLoading, areasFetchSuccess, areasFetchFail, areasLogout, reportAreaSuccess } = slice.actions
export default slice.reducer;


// Actions
export const fetchAreas = () => {
    return dispatch => {
        dispatch({
            type: "apiCall", payload: {
                url: "/areas/",
                method: "GET",
                auth: true,
                onLoading: areasFetchLoading.type,
                onSuccess: areasFetchSuccess.type,
                onError: areasFetchFail.type
            }
        })
    };
};

export const reportArea = (location, size, link, image, description) => {
  return dispatch => {
      const fd = new FormData();
        fd.append('name', location);
        fd.append('description', description);
        fd.append('location_link', link);
        fd.append('location_name', location);
        fd.append('owner', 1);
        fd.append('size', size);
        if (typeof image !== 'string') fd.append('image', image);
      dispatch({
          type: "apiCall",
          payload: {
              url: "/areas/",
              method: "POST",
              auth: true,
              data: fd,
              onSuccess: reportAreaSuccess.type,
          }
        }
      );
  };
};
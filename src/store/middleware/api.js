import axios from "axios";
import { toast } from 'react-toastify';
import { store } from "../../index";
import regeneratorRuntime from "regenerator-runtime";
import { authLogout } from "../auth";
import { maintenanceStart, maintenanceStop } from "../version";


const api = ({ dispatch }) => next => async action => {
    if (action.type !== "apiCall") return next(action);
    const token = store.getState().auth.token;
    const maintenance = store.getState().version.maintenance;
    const headers = { 'Content-Type': 'application/json' };
    if (action.payload.auth) headers.Authorization = 'Token ' + token
    const { url, method, data, onSuccess, onError, onLoading } = action.payload
    if (onLoading) dispatch({ type: onLoading })
    try {
        const response = await axios.request({
            baseURL: "/api/",
            headers,
            url,
            data,
            method
        });
        if (maintenance) dispatch({ type: maintenanceStop.type })
        if (response.data.version && store.getState().version.name !== response.data.version && store.getState().version.name !== null) {
            toast.info('🔖 Eine neue Version wurde veröffentlicht. ' + action.payload.version + ' \n Wir melden dich in 5 Sekunden ab.', {
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
            });
            setTimeout(function () {
                dispatch(authLogout())
                document.cookie.split(";").forEach(function (c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
                location.reload();
            }, 5000);

        }
        dispatch({ type: onSuccess, payload: response.data });

    } catch (error) {
        try {
            if (error.response.status === 502) dispatch({ type: maintenanceStart.type })
            else {
                toast.error('💥 Ein Fehler bei der Verbindung ist aufgetreten. ' + error.message, {
                    position: "bottom-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                });
                if (onError) dispatch({ type: onError, payload: error });
            }
        }
        catch (e) {
            if (error.message === "Network Error") dispatch({ type: maintenanceStart.type })
        }

    }
};

export default api;
import { createSlice } from "@reduxjs/toolkit";
import { toast } from 'react-toastify';
import { browserHistory } from 'react-router'



const initialState = {
    first_name: "",
    last_name: "",
    image: "",
    image_url: "",
    profile: null,
    username: "",
    user_id: null,
    email: '',
    location: 0,
    profile_id: null,
    is_staff: false,
    error: null,
    isLoading: false
};

const slice = createSlice({
    name: 'profile',
    initialState,
    reducers: {
        profileFetchLoading: (profile) => {
            profile.isLoading = true
        },
        profileFetchSuccess: (profile, action) => {
            const p = action.payload['results'][0]
            profile.first_name = p.first_name,
                profile.last_name = p.last_name,
                profile.username = p.username,
                profile.image = p.image,
                profile.image_url = p.image_url,
                profile.email = p.email,
                profile.location = p.current_position,
                profile.profile_id = p.id,
                profile.is_staff = p.is_staff,
                profile.user_id = p.user,
                profile.isLoading = false
        },
        profileFetchFail: (profile, action) => {
            profile.isLoading = false
            profile.error = action.payload.error
        },
        profileUpdateSuccess: (profile, action) => {
            const p = action.payload
            profile.first_name = p.first_name,
                profile.last_name = p.last_name,
                profile.username = p.username,
                profile.image = p.image,
                profile.image_url = p.image_url,
                profile.email = p.email,
                profile.location = p.current_position,
                profile.profile_id = p.id,
                profile.is_staff = p.is_staff,
                profile.user_id = p.user
                toast.success('👍 Dein Profil wurde aktualisiert.', {
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                });
        },
        profileLogout: state => initialState
    }
});

export const { profileFetchLoading, profileFetchSuccess, profileFetchFail, profileUpdateSuccess, profileLogout } = slice.actions
export default slice.reducer;

export const fetchProfile = () => {
    return dispatch => {
        dispatch({
            type: "apiCall", payload: {
                url: "/profiles",
                auth: true,
                onSuccess: profileFetchSuccess.type,
                onLoading: profileFetchLoading.type
            }
        })

    };
};

export const updateProfile = (profile, id) => {
    const { email, first_name, last_name, location, image, profile_id } = profile
    return dispatch => {
        const fd = new FormData();
        fd.append('email', email);
        fd.append('first_name', first_name);
        fd.append('last_name', last_name);
        fd.append('current_position', location);
        if (typeof image !== 'string') fd.append('image', image);
        dispatch({
            type: "apiCall", payload: {
                url: "/profiles/" + id,
                method: "PATCH",
                auth: true,
                data: fd,
                onSuccess: profileUpdateSuccess.type,
            }
        })
    };
};
import { createSlice } from "@reduxjs/toolkit";
import { createSelector } from 'reselect'


const slice = createSlice({
    name: 'projects',
    initialState: {
        subscribed: {},
        list: [],
        isLoading: false,
        detailsIsLoading: false,
        detailsLoaded: false,
        imagesLoaded: false,
        areasLoaded: false,
        subscribersLoaded: false,
        mebersLoaded: false,
        newsLoaded: false,
        details: [],
        selected: 0

    },
    reducers: {
        projectsFetchLoading: (projects) => {
            projects.isLoading = true
        },
        projectsFetchSuccess: (projects, action) => {
            projects.list = action.payload.results
            projects.isLoading = false
        },
        projectsFetchFail: (projects) => {
            projects.isLoading = false
        },
        subProjectsFetchLoading: (projects) => {
            projects.subIsLoaded = false
        },
        subProjectsFetchSuccess: (projects, action) => {
            action.payload.results.map(project => {
                projects.subscribed[project.id] = project
            })
            projects.subIsLoaded = true
        },
        subProjectsFetchFail: (projects) => {
            projects.subIsLoading = false
        },
        projectDetailsSelected: (projects, action) => {
            projects.selected = parseInt(action.payload)
        },
        resetProjectDetailsLoad: (projects) => {
            projects.detailsLoaded = false
            projects.imagesLoaded = false
            projects.areasLoaded = false
            projects.subscribersLoaded = false
        },
        projectDetailsFetchLoading: (projects) => {
            projects.detailsIsLoading = true
            projects.detailsLoaded = false
        },
        projectDetailsFetchSuccess: (projects, action) => {
            const p = action.payload
            projects.details[p.id] = action.payload
            projects.details[p.id].images = []
            projects.details[p.id].areas = []
            projects.details[p.id].subscribers = []
            projects.details[p.id].members = []
            projects.details[p.id].news = []
            projects.selected = p.id
            projects.detailsIsLoading = false
            projects.detailsLoaded = true

        },
        projectDetailsFetchFail: (projects) => {
            projects.detailsIsLoading = false
        },
        projectImagesFetchLoading: (projects) => {
            projects.details[projects.selected] = { ...projects.details[projects.selected], imagesLoaded: false }

        },
        projectImagesFetchSuccess: (projects, action) => {
            let imgs = [];
            const p = action.payload.results
            p.map(function (value) {
                imgs.push(value.image)
                projects.details[projects.selected] = { ...projects.details[projects.selected], images: imgs }
            });
            projects.imagesLoaded = true;

        },
        projectAreasFetchLoading: (projects, action) => {
            projects.details[projects.selected] = { ...projects.details[projects.selected], areasLoaded: false }

        },
        projectAreasFetchSuccess: (projects, action) => {
            let areas = [];
            const p = action.payload.results
            p.map(function (value) {
                areas.push({
                    id: value.id,
                    name: value.name,
                    image: value.image,
                    size: value.size,
                    location_name: value.location_name
                })
                projects.details[projects.selected] = { ...projects.details[projects.selected], areas: areas }
            });

            projects.areasLoaded = true;
        },
        projectNewsFetchLoading: (projects, action) => {
            projects.details[projects.selected] = { ...projects.details[projects.selected], newsLoaded: false }

        },
        projectNewsFetchSuccess: (projects, action) => {
            let news = [];
            const p = action.payload.results
            p.map(function (value) {
                news.push({
                    id: value.id,
                    title: value.title,
                    body: value.body,
                    image: value.image,
                    date: value.date
                })
                projects.details[projects.selected] = { ...projects.details[projects.selected], news: news }
            });

            projects.newsLoaded = true;
        },
        projectSubscribersFetchLoading: (projects, action) => {
            projects.details[projects.selected] = { ...projects.details[projects.selected], subscribersLoaded: false }
        },
        projectSubscribersFetchSuccess: (projects, action) => {
            let subs = [];
            const p = action.payload.results;
            p.map(function (value) {
                subs.push(value.profile)
                projects.details[projects.selected] = { ...projects.details[projects.selected], subscribers: subs }
            });
            projects.subscribersLoaded = true;

        },
        projectSub: (projects, action) => {
            projects.subscribed[projects.selected] = projects.details[projects.selected]
        },
        projectUnsub: (projects, action) => {
            delete projects.subscribed[projects.selected]
        },
        projectsSubLogout: (projects) => {
            projects.subscribed = {}
        },
        projectMembersFetchLoading: (projects) => {
            projects.details[projects.selected] = { ...projects.details[projects.selected], membersLoaded: false }
        },
        projectMembersFetchSuccess: (projects, action) => {
            let members = [];
            const p = action.payload;
            p.map(function (value) {
                members.push(value)
                projects.details[projects.selected] = { ...projects.details[projects.selected], members: members }
            });
            projects.membersLoaded = true;
        },
        projectMemberJoinSuccess: (projects, action) => {

        },
    }
});

export const {
    projectsFetchLoading, projectsFetchSuccess, projectsFetchFail,
    subProjectsFetchLoading, subProjectsFetchSuccess, subProjectsFetchFail,
    projectDetailsSelected, resetProjectDetailsLoad, projectsSubLogout,
    projectDetailsFetchLoading, projectDetailsFetchSuccess, projectDetailsFetchFail,
    projectImagesFetchSuccess, projectAreasFetchSuccess, projectSubscribersFetchSuccess, projectMembersFetchLoading,
    projectImagesFetchLoading, projectAreasFetchLoading, projectSubscribersFetchLoading, projectMembersFetchSuccess,
    projectNewsFetchLoading, projectNewsFetchSuccess,
    projectSub, projectUnsub, projectMemberJoinSuccess
} = slice.actions
export default slice.reducer;


// Projects actions
export const fetchProjects = (limit) => {
    return dispatch => {
        dispatch({
            type: "apiCall", payload: {
                url: "/projects?limit=" + limit,
                method: "GET",
                onLoading: projectsFetchLoading.type,
                onSuccess: projectsFetchSuccess.type,
                onError: projectsFetchFail.type
            }
        })
    };
};


export const fetchSubProjects = () => {
    return dispatch => {
        dispatch({
            type: "apiCall", payload: {
                url: "/projects?subscribed=true",
                method: "GET",
                auth: true,
                onLoading: subProjectsFetchLoading.type,
                onSuccess: subProjectsFetchSuccess.type,
                onError: subProjectsFetchFail.type
            }
        })
    };
};


// Project details actions
export const fetchProjectDetails = (id, auth) => {
    return dispatch => {
        dispatch(projectDetailsSelected(id));
        dispatch({
            type: "apiCall", payload: {
                url: "/projects/" + id,
                method: "GET",
                onLoading: projectDetailsFetchLoading.type,
                onSuccess: projectDetailsFetchSuccess.type,
                onError: projectDetailsFetchFail.type
            }
        })

        dispatch(fetchProjectDetailsData(id, auth))
    }
}

export const fetchProjectDetailsData = (id, auth) => {
    return dispatch => {
        dispatch({
            type: "apiCall", payload: {
                url: "/projects/" + id + "/images/",
                method: "GET",
                onSuccess: projectImagesFetchSuccess.type,
                onLoading: projectImagesFetchLoading.type
            }
        })
        dispatch({
            type: "apiCall", payload: {
                url: "/projects/" + id + "/subscribers/",
                method: "GET",
                onSuccess: projectSubscribersFetchSuccess.type,
                onLoading: projectSubscribersFetchLoading.type
            }
        })
        dispatch({
            type: "apiCall", payload: {
                url: "/projects/" + id + "/areas/",
                method: "GET",
                onSuccess: projectAreasFetchSuccess.type,
                onLoading: projectAreasFetchLoading.type
            }
        })
        dispatch({
            type: "apiCall", payload: {
                url: "/project_mebers_details/" + id,
                method: "GET",
                auth,
                onSuccess: projectMembersFetchSuccess.type,
                onLoading: projectMembersFetchLoading.type
            }
        })
        dispatch({
            type: "apiCall", payload: {
                url: "/projects/" + id + "/news/",
                method: "GET",
                auth,
                onSuccess: projectNewsFetchSuccess.type,
                onLoading: projectNewsFetchLoading.type
            }
        })
    }
}


// Subscribe actions
export const subscribeProject = (id) => {
    return dispatch => {
        dispatch({
            type: "apiCall", payload: {
                url: "/projects/" + id + "/subscribers/manage/",
                auth: true,
                method: "POST",
                onSuccess: projectSub.type,
            }
        })
    }
}

export const unsubscribeProject = (id) => {
    return dispatch => {
        dispatch({
            type: "apiCall", payload: {
                url: "/projects/" + id + "/subscribers/manage/",
                method: "DELETE",
                auth: true,
                onSuccess: projectUnsub.type,
            }
        })
    }
}

export const joinProject = (id, role, profile) => {
    return dispatch => {
        dispatch({
            type: "apiCall", payload: {
                url: "/projects/" + id + "/members/",
                method: "POST",
                data: { "role": role, "profile": profile, "project": id },
                auth: true,
                onSuccess: projectMemberJoinSuccess.type,
            }
        })
    }
}

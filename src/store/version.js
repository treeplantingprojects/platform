import { createSlice } from "@reduxjs/toolkit";
import { toast } from "react-toastify";


const slice = createSlice({
    name: 'version',
    initialState: {
        name: null,
        maintenance: false
    },
    reducers: {
        versionFetchSuccess: (version, action) => {
            version.name = action.payload.version
        },
        maintenanceStart: (version) => {
            version.maintenance = true
        },
        maintenanceStop: (version) => {
            version.maintenance = false
        }
    }
});

export const { versionFetchSuccess, maintenanceStart, maintenanceStop } = slice.actions
export default slice.reducer;

// Actions
export const fetchVersion = () => {
    return dispatch => {
        dispatch({
            type: "apiCall", payload: {
                url: "/version",
                method: "GET",
                onSuccess: versionFetchSuccess.type,
            }
        })
    };
};

export const checkVersionTimeout = versionExpirationTime => {

    return dispatch => {
        setTimeout(() => {
            dispatch(versionCheckState());
        }, versionExpirationTime * 1000);
    };
};

export const versionCheckState = () => {
    return dispatch => {
        const expirationDate = new Date(localStorage.getItem("versionExpirationDate"));
        if (expirationDate <= new Date()) {
            dispatch(fetchVersion());
            const versionExpirationDate = new Date(new Date().getTime() + 10 * 1000);
            localStorage.setItem("versionExpirationDate", versionExpirationDate);
            dispatch(
                checkVersionTimeout(
                    (expirationDate.getTime() - new Date().getTime()) / 1000
                )
            );
        } else {
            dispatch(
                checkVersionTimeout(
                    (expirationDate.getTime() - new Date().getTime()) / 1000
                )
            );
        }
    };
};

